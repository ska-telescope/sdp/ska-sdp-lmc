include .make/base.mk
include .make/python.mk
include .make/oci.mk
include .make/dependencies.mk

PROJECT_NAME = ska-sdp-lmc
PROJECT_PATH = ska-telescope/sdp/ska-sdp-lmc

ARTEFACT_TYPE = oci
CHANGELOG_FILE = CHANGELOG.rst