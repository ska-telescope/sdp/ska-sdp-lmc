SDP Local Monitoring and Control (Tango Devices)
================================================

This package contains the Tango device servers to control the SKA SDP.

Install with:

.. code:: bash

   poetry install

If you have Tango set up locally, you can run the devices with:

.. code:: bash

   SDPController <instance name> [-v4]

or:

.. code:: bash

   SDPSubarray <instance name> [-v4]

Standard CI machinery
---------------------

This repository is set up to use the
`Makefiles <https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile>`__
and `CI jobs <https://gitlab.com/ska-telescope/templates-repository>`__
maintained by the System Team. For any questions, please look at the
documentation in those repositories or ask for support on Slack in the
#team-system-support channel.

To keep the Makefiles up to date in this repository, follow the
instructions at:
https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile#keeping-up-to-date

Contributing to this repository
-------------------------------

`Black <https://github.com/psf/black>`__,
`isort <https://pycqa.github.io/isort/>`__, and various linting tools
are used to keep the Python code in good shape. Please check that your
code follows the formatting rules before committing it to the
repository. You can apply Black and isort to the code with:

.. code:: bash

   make python-format

and you can run the linting checks locally using:

.. code:: bash

   make python-lint

The linting job in the CI pipeline does the same checks, and it will
fail if the code does not pass all of them.

Creating a new release
----------------------

When you are ready to make a new release:

- Check out the master branch
- Update the version number with

  - ``make bump-patch-release``,
  - ``make bump-minor-release``, or
  - ``make bump-major-release``

- Manually set the version number in

  - ``src/ska_sdp_lmc/release.py``

- Update ``CHANGELOG.md``
- Create the git tag with ``make git-create-tag``
- Push the changes with ``make git-push-tag``
