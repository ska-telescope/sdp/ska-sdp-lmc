FROM artefact.skao.int/ska-build-python:0.1.1 AS build

WORKDIR /build

COPY pyproject.toml poetry.lock ./
COPY src ./src/

ENV POETRY_VIRTUALENVS_CREATE=false

RUN poetry install --no-root --only main
RUN pip install --no-compile .

FROM artefact.skao.int/ska-python:0.1.2

WORKDIR /app

COPY --from=build /usr/local/ /usr/local/

ENTRYPOINT ["SDPController"]
CMD ["0", "-v4"]
