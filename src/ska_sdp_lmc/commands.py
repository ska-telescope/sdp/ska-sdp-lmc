"""Functions for creating Tango device commands."""

import functools
import inspect
import json
import logging
from typing import Callable

from ska_ser_log_transactions import transaction
from tango.server import command

from .exceptions import raise_exception
from .feature_toggle import FeatureToggle
from .tango_logging import log_transaction_id

LOG = logging.getLogger(__name__)

FEATURE_ALL_COMMANDS_HAVE_ARGUMENT = FeatureToggle(
    "all_commands_have_argument", False
)


def command_transaction(argdesc: str | None = None):
    """
    Create a decorator for device command methods to add transaction
    processing.

    If called with the description of the argument, it creates a decorator that
    passes the transaction ID and the argument to the underlying method,
    otherwise the decorator passes only the transaction ID.

    :param argdesc: description of argument

    """

    def _decorator(command_method: Callable):
        # Define a wrapper that takes an optional string argument.

        @functools.wraps(command_method)
        def wrapper(self, params_json="{}"):
            name = command_method.__name__
            LOG.debug("command %s device %s", name, type(self).__name__)
            params = json.loads(params_json)

            with transaction(name, params, logger=LOG) as txn_id:
                with log_transaction_id(txn_id):
                    LOG.debug("Execute command %s", name)
                    if argdesc:
                        ret = command_method(self, txn_id, params_json)
                    else:
                        ret = command_method(self, txn_id)
            return ret

        # Decorate with command to create the device command

        if argdesc:
            # Create command with a string argument and use the supplied
            # description
            desc = (
                f"JSON string containing {argdesc} and optional transaction ID"
            )
            wrapper = command(f=wrapper, dtype_in=str, doc_in=desc)
        elif FEATURE_ALL_COMMANDS_HAVE_ARGUMENT.is_active():
            # Create command with a string argument and a generic description
            desc = "JSON string containing optional transaction ID"
            wrapper = command(f=wrapper, dtype_in=str, doc_in=desc)
        else:
            # Create command with no argument for backwards compatibility
            wrapper = command(f=wrapper, dtype_in=None)

        return wrapper

    return _decorator


def raise_command_not_allowed(message):
    """
    Raise a command-not-allowed exception.

    :param message: error message
    :raises: tango.DevFailed

    """
    reason = "API_CommandNotAllowed"
    origin = inspect.stack()[1].function
    raise_exception(reason, message, origin)


def raise_command_failed(message):
    """
    Raise a command-failed exception.

    :param message: error message
    :raises: tango.DevFailed

    """
    reason = "API_CommandFailed"
    origin = inspect.stack()[1].function
    raise_exception(reason, message, origin)


def command_allowed_state(state, allowed):
    """
    Check if command is allowed in current device state.

    If command is not allowed, raises a command-not-allowed exception.

    :param state: current device state
    :param allowed: list of allowed device states
    :raises: tango.DevFailed

    """
    if state not in allowed:
        message = f"Command not allowed in device state {state}"
        raise_command_not_allowed(message)


def command_allowed_obs_state(obs_state, allowed):
    """
    Check if command is allowed in current obsState.

    If command is not allowed, raises a command-not-allowed exception.

    :param obs_state: current obsState
    :param allowed: list of allowed obsStates
    :raises: tango.DevFailed

    """
    if obs_state not in allowed:
        message = f"Command not allowed in obsState {obs_state.name}"
        raise_command_not_allowed(message)
