"""SDP Tango device base class."""

import logging
import os
import signal
import threading
import time
from pathlib import Path
from typing import Callable, Type

from ska_control_model import AdminMode, HealthState
from tango import (
    AttrWriteType,
    AutoTangoMonitor,
    DevState,
    EnsureOmniThread,
    Util,
)
from tango.server import Device, attribute, command

from .config import BaseState
from .feature_toggle import FeatureToggle
from .release import VERSION

LOG = logging.getLogger(__name__)

EL_THREAD_NAME = "EventLoop"
FEATURE_EVENT_LOOP = FeatureToggle("event_loop", True)
# Event loop interval - also used by Kubernetes liveness probe logic
LOOP_INTERVAL = float(os.getenv("SDP_LMC_LOOP_INTERVAL", "10"))

# Liveness file name - must match the name used in the Kubernetes
# Pod yaml probe configuration!
LIVENESS_FILE = Path(os.getenv("SDP_LMC_LIVENESS_FILE", "/tmp/alive"))


def event_loop_exception(args):
    """
    Handle an uncaught exception in the event loop.

    :param args: as supplied by the threading exception hook
    """
    if threading.current_thread().name != EL_THREAD_NAME:
        return

    # Event loop death is treated as fatal.
    # The Tango loop needs to be stopped for the program to exit.
    # It would be better if a clean way to shut down Tango was found.
    LOG.critical(
        "Event loop thread stopped with %s: %s",
        args.exc_type.__name__,
        args.exc_value,
    )
    os.kill(os.getpid(), signal.SIGTERM)


threading.excepthook = event_loop_exception


class SDPDevice(Device):
    """SDP Tango device base class."""

    # pylint: disable=invalid-name
    # pylint: disable=attribute-defined-outside-init
    # pylint: disable=too-many-instance-attributes

    # ----------
    # Attributes
    # ----------

    versionId = attribute(
        label="Version",
        dtype=str,
        access=AttrWriteType.READ,
        doc="The version of the device",
    )

    adminMode = attribute(
        label="Admin mode",
        dtype=AdminMode,
        access=AttrWriteType.READ_WRITE,
        doc="Device admin mode",
    )

    healthState = attribute(
        label="Health state",
        dtype=HealthState,
        access=AttrWriteType.READ,
        doc="Device health state",
    )

    # ---------------
    # General methods
    # ---------------

    def __init__(self, cl, name):
        self._initialised = False
        super().__init__(cl, name)

    def init_device(self):
        """Initialise the device."""

        LOG.info("%s initialising", self.get_name())
        super().init_device()

        # Enable change and archive events on attributes
        for attr_name in ("State", "adminMode", "healthState", "versionId"):
            self.set_change_event(attr_name, True)
            self.set_archive_event(attr_name, True)

        # Initialise private values of attributes
        self._version_id = VERSION
        self._admin_mode = AdminMode.ONLINE
        self._health_state = HealthState.OK

        # Push versionId events here because it is only set when the device
        # starts/restarts
        self.push_change_event("versionId", VERSION)
        self.push_archive_event("versionId", VERSION)

        # Subclasses set the configuration.
        self._config = None

    def delete_device(self):
        """Delete the device."""
        name = self.get_name()
        LOG.info("%s deleting", name)

        self._stop_event_loop()
        self._config.close()

        super().delete_device()
        LOG.info("%s deleted", name)

    @property
    def initialised(self) -> bool:
        """
        Check if device is initialised.

        :return: true if it is
        """
        return self._initialised

    @initialised.setter
    def initialised(self, value: bool) -> None:
        """
        Set the device initialised status.

        :param value: to set
        """
        LOG.info(
            "Initialisation state changed from %s to %s",
            self.initialised,
            value,
        )
        self._initialised = value

    # -----------------
    # Attribute methods
    # -----------------

    def read_versionId(self) -> str:
        """Return server version."""
        return self._version_id

    def read_adminMode(self) -> AdminMode:
        """
        Read admin mode of the device.

        :return: Admin mode of the device
        """
        return self._admin_mode

    def write_adminMode(self, value: AdminMode) -> None:
        """
        Write admin mode of the device.

        :param value: Admin mode value
        """
        if isinstance(value, int):
            value = AdminMode(value)
        for txn in self._config.txn():
            txn.admin_mode = value

    def read_healthState(self) -> HealthState:
        """
        Read health state of the device.

        :return: Health state of the device
        """
        return self._health_state

    # -------------------------
    # Attribute-setting methods
    # -------------------------

    def _set_state(self, value: DevState) -> None:
        """Set device state."""
        LOG.debug(
            "Called _set_state %s -> %s", self.get_state().name, value.name
        )
        if self.get_state() != value:
            LOG.info("Setting device state to %s", value.name)
            self.set_state(value)
            self.push_change_event("State", self.get_state())
            self.push_archive_event("State", self.get_state())

    def _set_admin_mode(self, value: AdminMode) -> None:
        """Set adminMode and push change and archive events."""
        if self._admin_mode != value:
            LOG.info("Setting adminMode to %s", value.name)
            self._admin_mode = value
            self.push_change_event("adminMode", self._admin_mode)
            self.push_archive_event("adminMode", self._admin_mode)

    def _set_health_state(self, value: HealthState) -> None:
        """Set healthState and push change and archive events."""
        if self._health_state != value:
            LOG.info("Setting healthState to %s", value.name)
            self._health_state = value
            self.push_change_event("healthState", self._health_state)
            self.push_archive_event("healthState", self._health_state)

    # ------------------
    # Event loop methods
    # ------------------

    def _start_event_loop(self):
        """Start event loop."""

        self._el_watcher = None
        self._el_exit = False

        # Add owner key.
        for txn in self._config.txn():
            txn.take_ownership()

        if FEATURE_EVENT_LOOP.is_active():
            # Start event loop in thread
            self._el_thread = threading.Thread(
                target=self.event_loop, name=EL_THREAD_NAME, daemon=True
            )
            self._el_thread.start()
        else:
            self._el_thread = None

            # Add command to manually update attributes
            cmd = command(f=self.update_attributes)
            self.add_command(cmd, True)

    def _stop_event_loop(self):
        """Stop event loop."""
        self._el_exit = True
        if self._el_watcher is not None:
            LOG.debug("Trigger watcher loop")
            self._el_watcher.trigger()
        if self._el_thread is not None:
            self._el_thread.join()
            self._el_thread = None

    def event_loop(self):
        """Event loop to update attributes."""

        # Use EnsureOmniThread to make it thread-safe under Tango
        with EnsureOmniThread():
            LOG.info("Starting event loop")
            LOG.debug("Event loop interval is %.1f seconds", LOOP_INTERVAL)
            while not self.initialised:
                LOG.info("Waiting for device initialisation")
                time.sleep(0.1)

            for watcher in self._config.watcher(timeout=LOOP_INTERVAL):
                # Expose watcher as an attribute so loop can be triggered
                # by another thread
                self._el_watcher = watcher
                LOG.debug("Watcher wake-up, exit %s", self._el_exit)
                if self._el_exit:
                    break

                # Update liveness file for Kubernetes probe
                LIVENESS_FILE.touch()

                # Use the Tango monitor lock to prevent attributes being
                # updated by this thread when a command is running
                with AutoTangoMonitor(self):
                    for txn in watcher.txn():
                        LOG.debug("Starting set attributes from config")
                        self._set_attr_from_config(txn)
                        LOG.debug("Finished set attributes from config")
            self._el_watcher = None
            LOG.info("Exiting event loop")

    def update_attributes(self):
        """
        Update the device attributes manually.

        This method is used during synchronous testing (when the event loop is
        not enabled).

        """
        LOG.info("Updating attributes")
        for txn in self._config.txn():
            self._set_attr_from_config(txn)

    def _update_attr_until_condition(self, condition: Callable):
        """
        Update attributes until condition is satisfied.

        This generic method is used by other methods to wait for specific
        conditions.

        :param condition: condition to exit update loop

        """
        for watcher in self._config.watcher():
            for txn in watcher.txn():
                self._set_attr_from_config(txn)
            if condition():
                break

    def _update_attr_until_state(self, values):
        """
        Update attributes until device state reaches one of the values.

        This is used as a substitute event loop inside a command.

        :param values: list of state values

        """
        self._update_attr_until_condition(lambda: self.get_state() in values)

    def _set_attr_from_config(self, txn: BaseState) -> None:
        """
        Set attributes from configuration.

        This is called by the event loop.

        :param txn: wrapped configuration transaction

        """
        # Recreate owner key if it has been deleted
        txn.take_ownership_if_not_alive()

        # Set the attributes
        self._set_attributes(txn)

        # Clear command if it is finished, which will trigger another trip
        # around the event loop
        if txn.command and txn.command_finished:
            LOG.info("Command %s is finished", txn.command)
            txn.command = None

    def _set_attributes(self, txn: BaseState) -> None:
        """
        Set attributes.

        This is called by _set_attr_from_config

        :param txn: wrapped configuration transaction
        """
        raise NotImplementedError(
            "_set_attributes must be implemented by subclass"
        )


def devices_initialised(cls: Type[SDPDevice]) -> None:
    """
    Callback from Tango to indicate the devices are initialised.

    :param cls: the device(s) class
    """
    name = cls.__name__
    LOG.info("%s devices are initialised", name)
    devices = Util.instance().get_device_list_by_class(name)
    LOG.debug("There are %s device servers", len(devices))
    for device in devices:
        device.initialised = True
