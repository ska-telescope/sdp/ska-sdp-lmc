"""Tango exceptions."""

import logging

from tango import ErrSeverity, Except

LOG = logging.getLogger(__name__)


def raise_exception(reason, desc, origin, severity=ErrSeverity.ERR):
    """Raise a Tango DevFailed exception.

    :param reason: Reason for the error.
    :param desc: Error description.
    :param origin: Error origin.
    :param severity: Error severity.

    """
    LOG.error("Raising DevFailed exception...")
    LOG.error("Reason: %s", reason)
    LOG.error("Description: %s", desc)
    LOG.error("Origin: %s", origin)
    LOG.error("Severity: %s", severity)
    Except.throw_exception(reason, desc, origin, severity)
