"""Release information for ska-sdp-lmc package."""

NAME = "ska-sdp-lmc"
VERSION = "1.0.0"
VERSION_INFO = VERSION.split(".")
AUTHOR = "SKA SDP Developers"
LICENSE = "License :: OSI Approved :: BSD License"
COPYRIGHT = "BSD-3-Clause"
