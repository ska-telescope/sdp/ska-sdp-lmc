"""SDP Controller Tango device."""

import json
import logging

from ska_control_model import AdminMode
from tango import AttrWriteType, DevState
from tango.server import attribute

from ..commands import command_allowed_state, command_transaction
from ..device import SDPDevice, devices_initialised
from ..tango_logging import (
    configure_root_logger,
    log_device_name_class,
    log_transaction_id,
)
from .config import ControllerState, controller_config

LOG = logging.getLogger(__name__)


@log_device_name_class
class SDPController(SDPDevice):
    """SDP Controller device class."""

    # pylint: disable=invalid-name
    # pylint: disable=attribute-defined-outside-init

    # ----------
    # Attributes
    # ----------

    components = attribute(
        label="SDP component status",
        dtype=str,
        access=AttrWriteType.READ,
        doc="Status information on SDP components.",
    )

    sdpVersion = attribute(
        label="SDP Version",
        dtype=str,
        access=AttrWriteType.READ,
        doc="Versions of the SDP and its components",
    )

    # ---------------
    # General methods
    # ---------------

    def init_device(self):
        """Initialise the device."""
        super().init_device()
        self.set_state(DevState.INIT)

        # Enable change and archive events on attributes
        for attr_name in ("sdpVersion", "components"):
            self.set_change_event(attr_name, True)
            self.set_archive_event(attr_name, True)

        # Initialise private values of attributes
        self._sdp_version = ""
        self._components = ""

        # Initialise configuration DB connection
        self._config = controller_config()

        # Create device state if it does not exist
        for txn in self._config.txn():
            txn.create_if_not_present(DevState.STANDBY, AdminMode.ONLINE)

        # Start event loop
        self._start_event_loop()

        LOG.info("%s initialised", self.get_name())

    # -----------------
    # Attribute methods
    # -----------------

    def read_sdpVersion(self) -> str:
        """Return SDP version information."""
        return self._sdp_version

    def read_components(self):
        """Get the current components attribute value"""
        return self._components

    # --------
    # Commands
    # --------

    @command_transaction()
    def On(self, transaction_id: str):
        """
        Turn the SDP on.

        :param transaction_id: transaction ID

        """
        for txn in self._config.txn():
            # Check if command is allowed
            command_allowed_state(txn.state, [DevState.OFF, DevState.STANDBY])
            # Execute command
            txn.transaction_id = transaction_id
            txn.command = "On"
            txn.state_commanded = DevState.ON

        self._update_attr_until_state([DevState.ON])

    @command_transaction()
    def Standby(self, transaction_id: str):
        """
        Set the SDP to standby.

        :param transaction_id: transaction ID

        """
        for txn in self._config.txn():
            # Check if command is allowed
            command_allowed_state(txn.state, [DevState.OFF, DevState.ON])
            # Execute command
            txn.transaction_id = transaction_id
            txn.command = "Standby"
            txn.state_commanded = DevState.STANDBY

        self._update_attr_until_state([DevState.STANDBY])

    @command_transaction()
    def Off(self, transaction_id: str):
        """
        Turn the SDP off.

        :param transaction_id: transaction ID

        """
        for txn in self._config.txn():
            # Check if command is allowed
            command_allowed_state(txn.state, [DevState.STANDBY, DevState.ON])
            # Execute command
            txn.transaction_id = transaction_id
            txn.command = "Off"
            txn.state_commanded = DevState.OFF

        self._update_attr_until_state([DevState.OFF])

    # ------------------
    # Event loop methods
    # ------------------

    def _set_attributes(self, txn: ControllerState) -> None:
        """
        Set attributes from configuration.

        This is called by the event loop.

        :param txn: configuration transaction

        """
        if txn.state_commanded is None:
            LOG.info("Cannot read controller state: attributes cannot be set")
            return

        with log_transaction_id(txn.transaction_id):
            self._set_state(txn.state)
            self._set_admin_mode(txn.admin_mode)
            self._set_health_state(txn.health_state)
            self._set_sdp_version(txn.sdp_version)
            self._set_components(txn.components)

    # -------------------------
    # Attribute-setting methods
    # -------------------------

    def _set_sdp_version(self, value: dict):
        """Set sdpVersion and push change and archive events."""
        value_str = json.dumps(value)
        if self._sdp_version != value_str:
            LOG.info("Setting sdpVersion attribute to %s", value_str)
            self._sdp_version = value_str
            self.push_change_event("sdpVersion", value_str)
            self.push_archive_event("sdpVersion", value_str)

    def _set_components(self, value: dict):
        """Set components and push change and archive events"""
        value_str = json.dumps(value)
        if self._components != value_str:
            LOG.info("Setting components attribute to %s", value_str)
            self._components = value_str
            self.push_change_event("components", value_str)
            self.push_archive_event("components", value_str)


def main(args=None, **kwargs):
    """Run server."""
    configure_root_logger()
    return SDPController.run_server(
        args=args,
        post_init_callback=(devices_initialised, [SDPController], {}),
        **kwargs,
    )
