"""SDP Controller configuration interface."""

import json

import ska_sdp_config
from ska_control_model import AdminMode, HealthState
from tango import DevState

from ..config import BaseState, new_config_db_client

FAULTY_COMP_MESSAGE = {
    "lmc-controller": "SDP Controller is OFFLINE.",
    "lmc-subarray": (
        "SDP Subarray is OFFLINE. " "It cannot be used for processing."
    ),
    "proccontrol": (
        "SDP Processing Controller is OFFLINE. "
        "Cannot start new processing blocks"
    ),
    "helmdeploy": (
        "SDP Helm Deployer is OFFLINE:\n"
        "- Cannot deploy new processing scripts.\n"
        "- Running scripts will not deploy execution engines.\n"
        "- Deployments are not cleaned up."
    ),
}


def controller_config() -> ska_sdp_config.Config:
    """
    Create config DB client for the controller.

    This uses the ControllerState class as a transaction wrapper.

    :returns: config DB client

    """
    component_name = "lmc-controller"
    return new_config_db_client(
        component_name=component_name,
        wrapper=ControllerState,
    )


class ControllerState(BaseState):
    """
    Controller state interface.

    This wraps the transaction to provide a convenient interface for
    interacting with the controller state in the configuration.

    :param txn: configuration transaction.
    :param component_name: component name.
    """

    def create_if_not_present(
        self, state: DevState, admin_mode: AdminMode
    ) -> None:
        """
        Create controller entry if it does not exist already.

        :param state: initial state
        :param admin_mode: initial admin mode

        """
        controller = self._txn.self.get()
        if controller is None:
            controller = {
                "transaction_id": None,
                "command": None,
                "command_time": None,
                "state_commanded": state.name,
                "admin_mode": admin_mode.name,
            }
            self._txn.self.create(controller)

    @property
    def command_finished(self) -> bool | None:
        """Is the command finished?"""
        if self.command is None:
            return None
        return True

    @property
    def health_state(self) -> HealthState:
        """
        Health state.

        - If all of the subarrays are offline then the health state is FAILED.
        - If some, but not all, subarrays are offline then the health state is
          DEGRADED.
        - If the processing controller or Helm deployer is offline, then the
          health state is DEGRADED.
        - Otherwise the health state is OK.
        """
        subarrays_offline = [
            not online
            for component, online in self.online.items()
            if component.startswith("lmc-subarray")
        ]

        if all(subarrays_offline):
            return HealthState.FAILED

        if any(subarrays_offline):
            return HealthState.DEGRADED

        if not (self.online["proccontrol"] and self.online["helmdeploy"]):
            return HealthState.DEGRADED

        return HealthState.OK

    @property
    def components(self) -> dict:
        """Component status."""
        status = {}
        for component, online in self.online.items():
            if online:
                status[component] = {"status": "ONLINE"}
            else:
                if component.startswith("lmc-subarray"):
                    message = FAULTY_COMP_MESSAGE["lmc-subarray"]
                else:
                    message = FAULTY_COMP_MESSAGE[component]
                status[component] = {"status": "OFFLINE", "error": message}

        return status

    @property
    def sdp_version(self) -> dict:
        """SDP version information."""
        # pylint: disable=protected-access
        value = self._txn.system.get()
        if value is None:
            return {"error": "SDP version information not available"}
        value_dict = json.loads(value.model_dump_json())
        return value_dict
