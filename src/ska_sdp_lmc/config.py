"""Base configuration interface."""

import logging
from datetime import datetime, timezone
from typing import Any, Callable

import ska_sdp_config
from ska_control_model import AdminMode
from tango import DevState

from .feature_toggle import FeatureToggle

LOG = logging.getLogger(__name__)

FEATURE_CONFIG_DB = FeatureToggle("config_db", True)


def now() -> datetime:
    """Current time (UTC)."""
    return datetime.now(timezone.utc)


def new_config_db_client(
    component_name: str,
    wrapper: Callable | None = None,
) -> ska_sdp_config.Config:
    """
    Create a config DB client.

    :param component_name: name of component
    :param wrapper: wrapper function for transactions
    :returns: config DB client

    """
    backend = "etcd3" if FEATURE_CONFIG_DB.is_active() else "memory"
    LOG.info("Using config DB %s backend", backend)
    owned_entity = ("component", component_name) if component_name else None
    return ska_sdp_config.Config(
        backend=backend,
        owned_entity=owned_entity,
        wrapper=wrapper,
    )


class BaseState:
    """
    Base state interface.

    This provides the methods for querying and creating the component owner
    entry which is common to the devices using the event loop. It is
    sub-classed to provide device-specific methods.

    :param txn: configuration transaction.
    :param component_name: component name
    """

    def __init__(self, txn: ska_sdp_config.config.Transaction):
        self._txn = txn

    def take_ownership(self):
        """Take ownership of the component."""
        self._txn.self.ownership.take()

    def take_ownership_if_not_alive(self):
        """Take ownership of the component if not already owned."""
        self._txn.self.take_ownership_if_not_alive()

    def _get(self, name: str, default: Any = None) -> Any | None:
        """
        Get item in component entry.
        """
        state = self._txn.self.get()
        if state is not None:
            return state.get(name, default)
        return default

    def _set(self, name: str, value: Any) -> None:
        """
        Set item in component entry.
        """
        component = self._txn.self.get()
        component[name] = value
        self._txn.self.update(component)

    @property
    def transaction_id(self) -> str | None:
        """Transaction ID."""
        return self._get("transaction_id")

    @transaction_id.setter
    def transaction_id(self, value: str | None) -> None:
        self._set("transaction_id", value)

    @property
    def command(self) -> str | None:
        """Current command."""
        return self._get("command")

    @command.setter
    def command(self, value: str | None) -> None:
        command_time = now().isoformat() if value is not None else None
        self._set("command", value)
        self._set("command_time", command_time)

    @property
    def command_time(self) -> datetime | None:
        """Command time."""
        time = self._get("command_time")
        return None if time is None else datetime.fromisoformat(time)

    @property
    def command_finished(self) -> bool | None:
        """Current command is finished."""
        raise NotImplementedError(
            "command_finished must be implemented by subclass"
        )

    @property
    def state_commanded(self) -> DevState | None:
        """Commanded device state."""
        value = self._get("state_commanded")
        return DevState.names[value] if value in DevState.names else None

    @state_commanded.setter
    def state_commanded(self, value: DevState) -> None:
        self._set("state_commanded", value.name)

    @property
    def state(self) -> DevState | None:
        """Device state."""
        if self.admin_mode in (
            AdminMode.OFFLINE,
            AdminMode.NOT_FITTED,
            AdminMode.RESERVED,
        ):
            return DevState.DISABLE
        return self.state_commanded

    @property
    def admin_mode(self) -> AdminMode | None:
        """Admin mode."""
        value = self._get("admin_mode")
        return AdminMode[value] if value in AdminMode.__members__ else None

    @admin_mode.setter
    def admin_mode(self, value: AdminMode) -> None:
        self._set("admin_mode", value.name)

    @property
    def online(self) -> dict:
        """Are components online?"""
        subarrays = self._txn.component.query_keys(
            component_name_prefix="lmc-subarray"
        )
        components = (
            "lmc-controller",
            *subarrays,
            "proccontrol",
            "helmdeploy",
        )
        return {
            component: self._txn.component(component).is_alive()
            for component in components
        }
