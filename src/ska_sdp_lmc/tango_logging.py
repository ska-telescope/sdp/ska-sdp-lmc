"""Standard logging for TANGO devices."""

import contextlib
import contextvars
import functools
import inspect
import logging
import os

import ska_ser_logging
import tango

# Logging level
LOG_LEVEL = os.getenv("SDP_LOG_LEVEL", "DEBUG")

# Context variables for logging information
_DEVICE_NAME = contextvars.ContextVar("device_name", default="")
_TRANSACTION_ID = contextvars.ContextVar("transaction_id", default="")


def configure_root_logger():
    """Configure root logger."""
    ska_ser_logging.configure_logging(LOG_LEVEL, tags_filter=TagsFilter)


@contextlib.contextmanager
def log_device_name(device_name: str):
    """
    Context manager to log with device name.

    :param device_name: device name
    """
    token = _DEVICE_NAME.set(device_name)
    try:
        yield
    except tango.DevFailed:
        # Reset device name before raising the exception
        _DEVICE_NAME.reset(token)
        raise
    _DEVICE_NAME.reset(token)


def log_device_name_method(method):
    """Decorator for method to log with device name."""

    @functools.wraps(method)
    def wrapper(self, *args, **kwargs):
        with log_device_name(self.get_name()):
            retval = method(self, *args, **kwargs)
        return retval

    return wrapper


def log_device_name_class(cls):
    """Decorator for class to log with device name."""

    for name, value in inspect.getmembers(cls):
        if not name.startswith("_") and inspect.isfunction(value):
            setattr(cls, name, log_device_name_method(value))

    return cls


@contextlib.contextmanager
def log_transaction_id(txn_id: str):
    """
    Context manager to log with transaction ID.

    :param txn_id: transaction ID

    """
    token = _TRANSACTION_ID.set(txn_id)
    try:
        yield
    except tango.DevFailed:
        # Reset transaction ID before raising the exception
        _TRANSACTION_ID.reset(token)
        raise
    _TRANSACTION_ID.reset(token)


class TagsFilter(logging.Filter):
    """Filter to set "tags" field on log record."""

    # pylint: disable=too-few-public-methods

    def filter(self, record: logging.LogRecord):
        """Set "tags" field from context variables."""
        tags = []
        if device_name := _DEVICE_NAME.get():
            tags.append(f"tango-device:{device_name}")
        if transaction_id := _TRANSACTION_ID.get():
            tags.append(transaction_id)
        record.tags = ",".join(tags)
        return True
