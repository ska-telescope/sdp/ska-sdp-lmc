"""SDP Subarray command validation and parsing."""

import json
import logging
import re

import jsonschema
import packaging
import packaging.specifiers
from ska_sdp_config.entity import Script
from ska_telmodel.schema import validate
from ska_telmodel.sdp import (
    SDP_ASSIGNRES_PREFIX,
    SDP_CONFIGURE_PREFIX,
    SDP_RELEASERES_PREFIX,
    SDP_SCAN_PREFIX,
)

from ..commands import raise_command_failed
from ..feature_toggle import FeatureToggle

LOG = logging.getLogger(__name__)

FEATURE_STRICT_VALIDATION = FeatureToggle("strict_validation", False)

MSG_VALIDATION_FAILED = "Configuration failed schema validation"

ID_REGEX = r"^{obj_type}\-[a-z0-9]+\-[0-9]{{8}}\-[a-z0-9]+$"
EB_ID_REGEX = re.compile(ID_REGEX.format(obj_type="eb"))
PB_ID_REGEX = re.compile(ID_REGEX.format(obj_type="pb"))


SCHEMA_VERSION_0_4 = "0.4"
SCHEMA_VERSION_0_5 = "0.5"
SCHEMA_VERSION_1_0 = "1.0"


def _convert_assign_res_to_v1_0(config):
    """
    Convert AssignResources configuration from schema version 0.4 or 0.5 to 1.0
    """
    config["interface"] = "https://schema.skao.int/ska-sdp-assignres/1.0"

    config_eb = config.get("execution_block")
    if config_eb is None:
        return config

    fields = config_eb.get("fields", [])

    for field in fields:
        phase_dir = field.get("phase_dir")

        target_name = field.get("field_id")
        old_ra = phase_dir.get("ra")
        old_dec = phase_dir.get("dec")

        new_phase_dir = {
            "target_name": target_name,
            "attrs": {"c1": old_ra[0], "c2": old_dec[0], "epoch": 2000.0},
            "reference_frame": "icrs",
        }

        field["phase_dir"] = new_phase_dir

    return config


def validate_assign_resources(txn, config_str):
    """
    Validate AssignResources command configuration.

    :param txn: Transaction
    :param config_str: configuration string in dict format
    :returns: EB and list of processing blocks

    """
    LOG.info("Validation process for assign resources started.")
    version, config = validate_json_config(
        config_str,
        SDP_ASSIGNRES_PREFIX,
        (
            SCHEMA_VERSION_0_4,
            SCHEMA_VERSION_0_5,
            SCHEMA_VERSION_1_0,
        ),
    )

    if config is None:
        # Validation has failed, so raise an error
        raise_command_failed(MSG_VALIDATION_FAILED)

    if version in (SCHEMA_VERSION_0_4, SCHEMA_VERSION_0_5):
        # Convert to version 1.0
        config = _convert_assign_res_to_v1_0(config)

    if "execution_block" in config.keys() and not EB_ID_REGEX.fullmatch(
        config["execution_block"]["eb_id"]
    ):
        raise_command_failed(
            "Execution block ID is invalid: "
            f"{config['execution_block']['eb_id']}"
        )

    if "processing_blocks" in config.keys():
        for pbc in config["processing_blocks"]:
            if not PB_ID_REGEX.fullmatch(pbc["pb_id"]):
                raise_command_failed(
                    f"Processing block ID is invalid: {pbc['pb_id']}"
                )
            # validate pb params
            param_validation_error = validate_pb_params(txn, pbc)
            if param_validation_error is not None:
                raise_command_failed(
                    f"Processing block parameter validations failed for ID "
                    f"{pbc['pb_id']}, with error {param_validation_error}."
                )

    if "execution_block" in config.keys():
        scan_types = config["execution_block"].get("scan_types", [])
        for scan in scan_types:
            if scan.get("derive_from") and scan.get("scan_type_id").startswith(
                "."
            ):
                raise_command_failed(
                    "scan_type_id cannot start with '.' "
                    "if it also derives from a default scan_type"
                )

    # Extract resources, EB, and PBs
    resources = config.get("resources")
    eblock = config.get("execution_block")
    pbs = config.get("processing_blocks")

    return resources, eblock, pbs


def validate_release_resources(config_str):
    """
    Validate ReleaseResources argument.

    :param config_str: configuration string
    :returns: resources

    """
    _, config = validate_json_config(
        config_str,
        SDP_RELEASERES_PREFIX,
        (SCHEMA_VERSION_0_4, SCHEMA_VERSION_0_5, SCHEMA_VERSION_1_0),
    )

    if config is None:
        # Validation has failed, so raise an error
        raise_command_failed(MSG_VALIDATION_FAILED)

    resources = config.get("resources")

    return resources


def validate_configure(config_str):
    """
    Validate Configure command configuration.

    :param config_str: configuration string
    :returns: new scan types and scan type

    """
    # Validate the configuration string against the schema
    _, config = validate_json_config(
        config_str,
        SDP_CONFIGURE_PREFIX,
        (
            SCHEMA_VERSION_0_4,
            SCHEMA_VERSION_0_5,
            SCHEMA_VERSION_1_0,
        ),
    )

    if config is None:
        # Validation has failed, so raise an error
        raise_command_failed(MSG_VALIDATION_FAILED)

    new_scan_types = config.get("new_scan_types")
    scan_type = config.get("scan_type")

    return new_scan_types, scan_type


def validate_scan(config_str):
    """
    Validate Scan command configuration.

    :param config_str: configuration string
    :returns: scan ID

    """
    # Validate the configuration string against the schema
    _, config = validate_json_config(
        config_str,
        SDP_SCAN_PREFIX,
        (
            SCHEMA_VERSION_0_4,
            SCHEMA_VERSION_0_5,
            SCHEMA_VERSION_1_0,
        ),
    )

    if config is None:
        # Validation has failed, so raise an error
        raise_command_failed(MSG_VALIDATION_FAILED)

    scan_id = config.get("scan_id")

    return scan_id


def validate_json_config(config_str, prefix, allowed):
    """
    Validate a JSON configuration string against a schema.

    :param config_str: JSON configuration string
    :param prefix: schema prefix
    :param allowed: allowed versions of the schema
    :returns: version and validated configuration, or both are set to None if
        validation fails

    """
    try:
        config = json.loads(config_str)
    except json.JSONDecodeError as error:
        LOG.error(
            "Unable to decode configuration string as JSON: %s", error.msg
        )
        return None, None

    if "interface" not in config:
        LOG.error("Command argument must contain 'interface' value")
        return None, None

    schema = config.get("interface")

    if FEATURE_STRICT_VALIDATION.is_active():
        strictness = 2
    else:
        strictness = 1

    try:
        validate(schema, config, strictness)
    except ValueError as error:
        LOG.error("Unable to validate JSON configuration: %s", str(error))
        return None, None

    version = schema[len(prefix) :]  # noqa: E203
    if version not in allowed:
        LOG.error("Schema version is not allowed: %s", version)
        return None, None

    LOG.debug("Successfully validated JSON configuration")

    return version, config


def validate_pb_params(txn, pb):
    """
    Valid processing block parameters against a schema
    stored in the script.

    :param txn: Subarray Transaction (config transaction
                is txn._txn)
    :param pb: Processing Block
    """

    try:
        script_key = Script.Key(**pb["script"])
    except KeyError:
        LOG.info(
            "No script associated with processing "
            "block %s, validation skipped.",
            pb["pb_id"],
        )
        return None
    try:
        # pylint:disable=protected-access
        script = txn._txn.script.get(script_key)
        schema = script.parameters
    except AttributeError:
        LOG.info(
            "No schema provided for processing "
            "block %s, validation skipped.",
            pb["pb_id"],
        )
        return None
    if not schema:
        LOG.info(
            "No schema provided for processing "
            "block %s, validation skipped.",
            pb["pb_id"],
        )
        return None

    try:
        jsonschema.validate(instance=pb["parameters"], schema=schema)
        return None

    except jsonschema.exceptions.ValidationError as error:
        LOG.error("Validate JSON configuration failed: %s", str(error))
        return error


def script_compatible_with_sdp(
    sdp_version: str | None,
    script_sdp_version: str | None,
) -> bool | None:
    """
    Check if script is compatible with the SDP.

    :param sdp_version: SDP version
    :param script_sdp_version: script's compatible SDP versions
    :returns: if script is compatible, or None if check is not possible

    """
    if not sdp_version or not script_sdp_version:
        return None
    sdp_version = packaging.version.Version(sdp_version)
    script_sdp_version = packaging.specifiers.SpecifierSet(script_sdp_version)
    # Use base version so that pre-release versions of the SDP, including
    # release candidates, are treated in the same way as the release
    return script_sdp_version.contains(sdp_version.base_version)
