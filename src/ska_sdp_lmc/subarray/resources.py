"""Functions for assigning and releasing resources."""


def assign(assigned: dict, resources: dict):
    """
    Assign resources.

    :param assigned: resources before assignment
    :param resources: resources to be assigned
    :returns: resources after assignment

    """
    for key, value in resources.items():
        if key in assigned:
            old_value = assigned[key]
        else:
            old_value = None
        if isinstance(value, int):
            new_value = assign_int(old_value, value)
        elif isinstance(value, list):
            new_value = assign_list(old_value, value)
        else:
            raise ValueError(f"Cannot assign {key} resources: {value}")
        assigned[key] = new_value
    return assigned


def release(assigned: dict, resources: dict):
    """
    Release resources.

    :param assigned: resources before release
    :param resources: resources to be released
    :returns: resources after release

    """
    for key, value in resources.items():
        if key in assigned:
            old_value = assigned[key]
        else:
            old_value = None
        if isinstance(value, int):
            new_value = release_int(old_value, value)
        elif isinstance(value, list):
            new_value = release_list(old_value, value)
        else:
            raise ValueError(f"Cannot release {key} resources: {value}")
        if old_value and not new_value:
            del assigned[key]
        else:
            assigned[key] = new_value
    return dict(assigned)


def assign_int(old, resources):
    """
    Assign integer resources.

    :param old: resources before assignment
    :param resources: resources to be assigned
    :returns: resources after assignment

    """
    if old is None:
        new = resources
    else:
        new = old + resources
    return new


def release_int(old, resources):
    """
    Release integer resources.

    :param old: resources before release
    :param resources: resources to be released
    :returns: resources after release

    """
    if old is None:
        new = None
    else:
        new = max(old - resources, 0)
    return new


def assign_list(old, resources):
    """
    Assign resources to list.

    :param old: resources before assignment
    :param resources: resources to be assigned
    :returns: resources after assignment

    """
    if old is None:
        new = resources
    else:
        new = old + [v for v in resources if v not in old]
    return new


def release_list(old, resources):
    """
    Release resources from list.

    :param old: resources before release
    :param resources: resources to be released
    :returns: resources after release

    """
    if old is None:
        new = None
    else:
        new = [v for v in old if v not in resources]
    return new
