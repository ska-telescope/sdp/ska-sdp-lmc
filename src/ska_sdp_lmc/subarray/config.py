"""SDP Subarray configuration interface."""

import logging
import os
from typing import Any

import ska_sdp_config
from ska_control_model import AdminMode, HealthState, ObsState
from ska_sdp_config.entity import ExecutionBlock, ProcessingBlock, Script
from tango import DevState

from ..commands import raise_command_failed
from ..config import BaseState, new_config_db_client, now
from . import resources as resources_mod
from .validation import script_compatible_with_sdp

LOG = logging.getLogger(__name__)

ASSIGN_RESOURCES_TIMEOUT = float(
    os.getenv("SDP_LMC_ASSIGN_RESOURCES_TIMEOUT", "60")
)
CONFIGURE_TIMEOUT = float(os.getenv("SDP_LMC_CONFIGURE_TIMEOUT", "60"))


def _get_subarray_id(device_name: str) -> str:
    """
    Get subarray ID.

    The ID is the number of the subarray device, which is extracted from
    the device name.

    :param device_name: device name
    :returns: subarray ID

    """
    _, family, member = device_name.split("/")

    if family == "subarray":
        # New-style device name
        subarray_id = member
    elif family == "elt":
        # Old-style device name
        number = member.split("_")[1]
        subarray_id = number.zfill(2)
    else:
        raise ValueError(
            f"Cannot extract subarray ID from device name: {device_name}"
        )

    return subarray_id


def subarray_config(device_name: str) -> ska_sdp_config.Config:
    """
    Create config DB client for the subarray.

    This uses the SubarrayState class as transaction wrapper.

    :param device_name: device name
    :returns: config DB client

    """
    subarray_id = _get_subarray_id(device_name)
    component_name = f"lmc-subarray-{subarray_id}"
    return new_config_db_client(
        component_name=component_name,
        wrapper=lambda txn: SubarrayState(txn, subarray_id),
    )


class SubarrayState(BaseState):
    """
    Subarray state interface.

    This wraps the transaction to provide a convenient interface for
    interacting with the subarray state in the configuration.

    :param txn: configuration transaction
    :param component_name: component name
    :param subarray_id: subarray ID
    """

    # pylint: disable=too-many-instance-attributes
    # pylint: disable=too-many-public-methods

    def __init__(self, txn, subarray_id: str):
        super().__init__(txn)
        self._id = subarray_id

    def create_if_not_present(
        self, state: DevState, obs_state: ObsState, admin_mode: AdminMode
    ) -> None:
        """
        Create subarray entry if it does not exist already.

        :param state: initial device state
        :param obs_state: initial observing state
        :param admin_mode: initial admin mode

        """
        subarray = self._txn.self.get()
        if subarray is None:
            subarray = {
                "transaction_id": None,
                "command": None,
                "command_time": None,
                "state_commanded": state.name,
                "obs_state_commanded": obs_state.name,
                "admin_mode": admin_mode.name,
                "resources": {},
                "eb_id": None,
            }
            self._txn.self.create(subarray)

    @property
    def command(self) -> str | None:
        """Current command."""
        return self._get("command")

    @command.setter
    def command(self, value: str | None) -> None:
        command_time = now().isoformat() if value is not None else None
        self._set("command", value)
        self._set("command_time", command_time)

    @property
    def command_finished(self) -> bool | None:
        """Command is finished."""
        if self.command is None:
            return None
        if self.command == "AssignResources":
            return self.eb_pb_realtime_status_ready
        if self.command == "Configure":
            return self.eb_pb_realtime_deployments_ready
        return True

    @property
    def health_state(self) -> HealthState:
        """
        Health state.

        - If processing controller or Helm deployer is offline, then the health
        state is DEGRADED.
        - If pb has status RUNNING while also containing error messages, then
        the healthstate is DEGRADED
        - Otherwise the health state is OK.
        """
        if not (self.online["proccontrol"] and self.online["helmdeploy"]):
            return HealthState.DEGRADED

        for state in self._eb_pb_realtime_states:
            if (
                state is not None
                and state.get("status") != "FAILED"
                and state.get("error_messages")
            ):
                LOG.info(
                    "pb status is not failed but there are error messages "
                    "present"
                )
                LOG.info("Setting healthstate to DEGRADED")
                return HealthState.DEGRADED

        return HealthState.OK

    @property
    def obs_state_commanded(self) -> ObsState | None:
        """Commanded obsState."""
        value = self._get("obs_state_commanded")
        return ObsState[value] if value in ObsState.__members__ else None

    @obs_state_commanded.setter
    def obs_state_commanded(self, value: ObsState) -> None:
        self._set("obs_state_commanded", value.name)

    def _have_pbs_failed(self):
        for pb_state in self._eb_pb_realtime_states:
            if pb_state is not None and pb_state.get("status") == "FAILED":
                return True
        return False

    @property
    def obs_state(self) -> ObsState | None:
        """obsState."""
        # pylint: disable=too-many-return-statements
        if self.command == "Abort":
            return ObsState.ABORTING

        if self.command == "Restart":
            return ObsState.RESTARTING

        # if command is not Abort or Restart, set the obsState to FAULT
        # if any of the associated pbs' state is FAILED
        if self._have_pbs_failed():
            return ObsState.FAULT

        if self.command == "AssignResources":
            if self._timed_out(ASSIGN_RESOURCES_TIMEOUT):
                LOG.warning("AssignResources has timed out")
                return ObsState.FAULT
            return ObsState.RESOURCING

        if self.command in ("ReleaseResources", "ReleaseAllResources"):
            return ObsState.RESOURCING

        if self.command == "Configure":
            if self._timed_out(CONFIGURE_TIMEOUT):
                LOG.warning("Configure has timed out")
                return ObsState.FAULT
            return ObsState.CONFIGURING

        return self.obs_state_commanded

    def _timed_out(self, timeout: float) -> bool:
        """Command has timed out."""
        time_elapsed = now() - self.command_time
        return time_elapsed.total_seconds() > timeout

    # Execution block related properties and methods

    @property
    def eb_id(self) -> str | None:
        """Execution block ID."""
        return self._get("eb_id")

    def _get_eb(self, name: str, default: Any = None) -> Any:
        """
        Get item from linked execution block entry.

        :param name: the name of the item
        :param default: default value
        :returns: the value of the item, or default value if not present

        """
        if self.eb_id is not None:
            eblock = self._txn.execution_block.get(self.eb_id)
            if eblock is not None:
                return getattr(eblock, name, default)
        return default

    def _get_eb_state(self, name: str, default: Any = None) -> Any:
        """
        Get item from linked execution block state.

        :param name: the name of the item
        :param default: default value
        :returns: the value of the item, or default value if not present
        """

        if self.eb_id is not None:
            eb_state = self._txn.execution_block.state(self.eb_id).get()
            if eb_state is not None:
                return eb_state.get(name, default)

        return default

    def _set_eb(self, name: str, value: Any) -> None:
        """
        Set item in execution block entry.

        :param name: the name of the item
        :param value: value to set

        """
        if self.eb_id is not None:
            eblock = self._txn.execution_block.get(self.eb_id)
            if eblock is None:
                message = f"Execution block {self.eb_id} does not exist"
                raise_command_failed(message)
            setattr(eblock, name, value)
            self._txn.execution_block.update(eblock)

    def _set_eb_state(self, name: str, value: Any) -> None:
        """
        Set item in execution block state.

        :param name: the name of the item
        :param value: value to set

        """
        if self.eb_id is not None:
            eb_state = self._txn.execution_block.state(self.eb_id).get()
            if eb_state is None:
                message = (
                    f"Execution block state for {self.eb_id} does not exist"
                )
                raise_command_failed(message)
            eb_state[name] = value
            self._txn.execution_block.state(self.eb_id).update(eb_state)

    @property
    def eb_scan_type(self) -> str | None:
        """Scan type."""
        return self._get_eb_state("scan_type")

    @eb_scan_type.setter
    def eb_scan_type(self, value: str | None) -> None:
        if self.eb_id is not None and value is not None:
            # Check if scan type is in configuration
            scan_types = self._get_eb("scan_types", default=[])
            st_ids = [st.get("scan_type_id") for st in scan_types]
            if value not in st_ids:
                message = f"Scan type {value} is not defined"
                raise_command_failed(message)
        self._set_eb_state("scan_type", value)

    @property
    def eb_scan_id(self) -> int | None:
        """Scan ID."""
        return self._get_eb_state("scan_id")

    @eb_scan_id.setter
    def eb_scan_id(self, value: int | None) -> None:
        self._set_eb_state("scan_id", value)

    @property
    def eb_scans(self) -> list[dict]:
        """Get list of completed scans."""
        return self._get_eb_state("scans", [])

    def eb_add_current_scan(self, status: str) -> None:
        """
        Add current scan to list of completed scans.

        :param status: status of scan

        """
        if self.eb_scan_type is None:
            raise_command_failed("scan type must not be None")
        if self.eb_scan_id is None:
            raise_command_failed("scan ID must not be None")
        scans = self.eb_scans
        scans.append(
            {
                "scan_id": self.eb_scan_id,
                "scan_type": self.eb_scan_type,
                "status": status,
            }
        )
        self._set_eb_state("scans", scans)

    @property
    def _eb_pb_realtime_states(self) -> list[dict]:
        """List of real-time processing block states."""
        pb_ids = self._get_eb("pb_realtime", default=[])
        return [
            self._txn.processing_block.state(pb_id).get() for pb_id in pb_ids
        ]

    @property
    def eb_pb_realtime_status_ready(self) -> bool:
        """All real-time processing blocks are ready."""
        return all(
            False if state is None else (state.get("status") == "READY")
            for state in self._eb_pb_realtime_states
        )

    @property
    def eb_pb_realtime_deployments_ready(self) -> bool:
        """All real-time processing deployments are ready."""
        return all(
            False if state is None else state.get("deployments_ready", False)
            for state in self._eb_pb_realtime_states
        )

    @property
    def eb_error_messages(self) -> dict | None:
        """Error Messages."""
        error_messages_dict = {}

        # Get pb_ids and their corresponding states as a dictionary
        pb_states = {
            pb_id: self._txn.processing_block.state(pb_id).get()
            for pb_id in self._get_eb("pb_realtime", default=[])
        }

        for pb_id, state in pb_states.items():
            if state:
                error_messages = state.get("error_messages", {})

                if error_messages:
                    error_messages_dict[pb_id] = error_messages
                elif pb_id in error_messages_dict:
                    del error_messages_dict[pb_id]

        return error_messages_dict

    @property
    def eb_receive_addresses(self) -> dict:
        """Receive addresses."""
        receive_addresses = {}
        # pylint: disable=too-many-nested-blocks
        for state in self._eb_pb_realtime_states:
            if state is not None:
                ra_pb = state.get("receive_addresses", {})
                for scan_type, beams in ra_pb.items():
                    if scan_type in receive_addresses:
                        for beam, values in beams.items():
                            if beam in receive_addresses[scan_type]:
                                receive_addresses[scan_type][beam].update(
                                    values
                                )
                            else:
                                receive_addresses[scan_type][beam] = values
                    else:
                        receive_addresses[scan_type] = beams
        return receive_addresses

    def create_eb(self, eb_dict: dict) -> None:
        """
        Create execution block.

        This creates the link from the subarray entry to the execution block
        entry, and copies the resources.

        :param eb_dict: execution block dictionary

        """
        eb_id = eb_dict.get("eb_id")

        # Create link from subarray to the EB
        self._set("eb_id", eb_id)
        # Create the EB
        eb_test = self._txn.execution_block.get(eb_id)
        if eb_test is not None:
            message = f"Execution block {eb_id} already exists"
            raise_command_failed(message)
        LOG.info("Creating execution block %s", eb_id)
        eb_dict.update({"key": eb_id})
        eblock = ExecutionBlock(**eb_dict)
        # Create link back from EB to subarray
        eblock.subarray_id = self._id
        # Copy subarray resources to EB
        eblock.resources = self.resources
        self._txn.execution_block.create(eblock)
        # Update representing state
        eb_state = self._txn.execution_block.state(eb_id).get()
        if eb_state is not None:
            message = f"Execution block state for {eb_id} already exists"
            raise_command_failed(message)
        # Create eb state
        eb_state = {
            "scan_type": None,
            "scan_id": None,
            "scans": [],
            "status": "ACTIVE",
        }
        self._txn.execution_block.state(eb_id).create(eb_state)

    def create_pbs(self, pbs: list[dict]) -> None:
        """
        Create processing blocks.

        This creates the link from the execution block to the processing blocks
        and vice versa.

        :param pbs: list of processing blocks (as dict)

        """
        # pylint: disable=too-many-locals
        # pylint: disable=too-many-branches

        # Check if EB is present
        if self.eb_id is None:
            message = "Cannot create processing blocks without execution block"
            raise_command_failed(message)

        # Check helm deployer and processing controller are
        # not OFFLINE. If either is, raise an exception.

        if not self.online["proccontrol"] or not self.online["helmdeploy"]:
            message = (
                "The processing controller, helm deployer, or both "
                "are OFFLINE: cannot start processing blocks."
            )
            raise_command_failed(message)

        # Create the PBs
        pb_realtime = self._get_eb("pb_realtime", default=[])
        pb_batch = self._get_eb("pb_batch", default=[])
        for pbc in pbs:
            pb_id = pbc.get("pb_id")
            if pbc.get("script"):
                script_key = Script.Key(**pbc.get("script"))
            else:
                script_key = None
            parameters = pbc.get("parameters", {})
            dependencies = pbc.get("dependencies", [])
            pb_test = self._txn.processing_block.get(pb_id)
            if pb_test is not None:
                message = f"Processing block {pb_id} already exists"
                raise_command_failed(message)
            script = self._txn.script.get(script_key)
            if not script:
                message = f"Processing script {script_key} is not defined"
                raise_command_failed(message)
            sdp_version = None
            if self._txn.system.get():
                sdp_version = self._txn.system.get().version
            compatible = script_compatible_with_sdp(
                sdp_version, script.sdp_version
            )
            if compatible is None:
                LOG.warning(
                    "Unable to check script compatability with SDP version %s",
                    sdp_version,
                )
            elif not compatible:
                message = (
                    f"Processing script {script_key} is not "
                    f"compatible with SDP version {sdp_version} "
                    f"(compatible SDP versions {script.sdp_version})"
                )
                raise_command_failed(message)
            LOG.info("Creating processing block %s", pb_id)
            kind = script_key.kind
            if kind == "realtime":
                pb_realtime.append(pb_id)
            elif kind == "batch":
                pb_batch.append(pb_id)
            else:
                LOG.warning("Unknown processing script kind: %s", kind)
            pblock = ProcessingBlock(
                key=pb_id,
                eb_id=self.eb_id,
                script=script_key,
                parameters=parameters,
                dependencies=dependencies,
            )
            self._txn.processing_block.create(pblock)
        self._set_eb("pb_realtime", pb_realtime)
        self._set_eb("pb_batch", pb_batch)

    def _end_eb(self, status: str) -> None:
        """
        End execution block.

        This sets the status of the EB, then removes the link from the subarray
        entry to the EB entry in the configuration. Setting the status will
        signal to the real-time processing blocks that they should terminate.
        The EB remains in the configuration, but it will no longer be
        accessible from the subarray state interface.

        :param status: status to set in execution block

        """
        # Set execution block values before breaking the link
        self._set_eb("subarray_id", None)
        self._set_eb_state("status", status)
        self.eb_scan_type = None
        self.eb_scan_id = None
        # This breaks the link
        self._set("eb_id", None)

    def finish_eb(self) -> None:
        """
        Finish execution block.

        This ends the execution block and sets its status to FINISHED.

        """
        self._end_eb("FINISHED")

    def cancel_eb(self) -> None:
        """
        Cancel execution block.

        This ends the execution block and sets its status to CANCELLED.

        """
        self._end_eb("CANCELLED")

    # Resources properties and methods

    @property
    def resources(self) -> dict:
        """Resources currently assigned to subarray."""
        return self._get("resources", default={})

    def assign_resources(self, resources: dict) -> None:
        """
        Assign resources.

        :param resources: resources to assign

        """

        self._set("resources", resources_mod.assign(self.resources, resources))

    def release_resources(self, resources: dict) -> None:
        """
        Release resources.

        :param resources: resources to release

        """
        self._set(
            "resources", resources_mod.release(self.resources, resources)
        )

    def release_all_resources(self) -> None:
        """Release all resources."""
        self._set("resources", {})
