"""SDP Subarray Tango device."""

import json
import logging

from ska_control_model import AdminMode, ObsState
from tango import AttrWriteType, DevState
from tango.server import attribute

from ..commands import (
    command_allowed_obs_state,
    command_allowed_state,
    command_transaction,
    raise_command_failed,
)
from ..device import SDPDevice, devices_initialised
from ..tango_logging import (
    configure_root_logger,
    log_device_name_class,
    log_transaction_id,
)
from .config import SubarrayState, subarray_config
from .validation import (
    validate_assign_resources,
    validate_configure,
    validate_release_resources,
    validate_scan,
)

LOG = logging.getLogger(__name__)


@log_device_name_class
class SDPSubarray(SDPDevice):
    """SDP Subarray device class."""

    # pylint: disable=invalid-name
    # pylint: disable=attribute-defined-outside-init
    # pylint: disable=too-many-instance-attributes
    # pylint: disable=too-many-public-methods

    # ----------
    # Attributes
    # ----------

    obsState = attribute(
        label="Observing state",
        dtype=ObsState,
        access=AttrWriteType.READ,
        doc="The device observing state.",
    )

    resources = attribute(
        label="Resources",
        dtype=str,
        access=AttrWriteType.READ,
        doc="Resources assigned to subarray as a JSON string.",
    )

    ebID = attribute(
        label="Execution block ID",
        dtype=str,
        access=AttrWriteType.READ,
        doc="Execution block ID.",
    )

    receiveAddresses = attribute(
        label="Receive addresses",
        dtype=str,
        access=AttrWriteType.READ,
        doc="Host addresses for visibility receive as a JSON string.",
    )

    scanType = attribute(
        label="Scan type",
        dtype=str,
        access=AttrWriteType.READ,
        doc="Scan type.",
    )

    scanID = attribute(
        label="Scan ID",
        dtype=int,
        access=AttrWriteType.READ,
        doc="Scan ID.",
        abs_change=1,
    )

    errorMessages = attribute(
        label="Error Messages",
        dtype=str,
        access=AttrWriteType.READ,
        doc="The error messages reported when there is an \
            error in the processing.",
    )

    # ---------------
    # General methods
    # ---------------

    def init_device(self):
        """Initialise the device."""
        super().init_device()
        self.set_state(DevState.INIT)

        # Enable change and archive events on attributes
        for attr_name in (
            "obsState",
            "resources",
            "ebID",
            "receiveAddresses",
            "scanType",
            "scanID",
            "errorMessages",
        ):
            self.set_change_event(attr_name, True)
            self.set_archive_event(attr_name, True)

        # Initialise private values of attributes
        self._obs_state = None
        self._resources = "null"
        self._eb_id = "null"
        self._receive_addresses = "null"
        self._scan_type = "null"
        self._scan_id = 0
        self._error_messages = "null"

        # Initialise configuration DB connection
        self._config = subarray_config(self.get_name())

        # Create device state if it does not exist
        for txn in self._config.txn():
            txn.create_if_not_present(
                DevState.OFF, ObsState.EMPTY, AdminMode.ONLINE
            )

        # Start event loop
        self._start_event_loop()

        LOG.info("%s initialised", self.get_name())

    # -----------------
    # Attribute methods
    # -----------------

    def read_obsState(self):
        """
        Get the obsState.

        :returns: the current obsState.

        """
        return self._obs_state

    def read_resources(self):
        """
        Get the resources assigned to the subarray.

        :returns: resources as JSON string

        """
        return self._resources

    def read_ebID(self):
        """
        Get the ID of the execution block assigned to the subarray.

        :returns: execution block ID

        """
        return self._eb_id

    def read_receiveAddresses(self):
        """
        Get the receive addresses.

        :returns: receive address map as JSON string

        """
        return self._receive_addresses

    def read_scanType(self):
        """
        Get the scan type.

        :returns: scan type ('null' = no scan type)

        """
        return self._scan_type

    def read_scanID(self):
        """
        Get the scan ID.

        :returns: scan ID (0 = no scan ID)

        """
        return self._scan_id

    def read_errorMessages(self):
        """
        Get the error messages.

        :returns: error messages map as JSON string

        """
        return self._error_messages

    # --------
    # Commands
    # --------

    @command_transaction()
    def On(self, transaction_id: str):
        """
        Turn the subarray on.

        :param transaction_id: transaction ID

        """
        for txn in self._config.txn():
            # Check if command is allowed
            command_allowed_state(txn.state, [DevState.OFF])
            command_allowed_obs_state(txn.obs_state, [ObsState.EMPTY])
            # Execute command
            txn.command = "On"
            txn.transaction_id = transaction_id
            txn.state_commanded = DevState.ON
            txn.obs_state_commanded = ObsState.EMPTY

        self._update_attr_until_state([DevState.ON])

    @command_transaction()
    def Off(self, transaction_id: str):
        """
        Turn the subarray off.

        :param transaction_id: transaction ID

        """
        for txn in self._config.txn():
            # Check if command is allowed
            command_allowed_state(txn.state, [DevState.ON])
            # Execute command
            txn.command = "Off"
            txn.transaction_id = transaction_id
            if txn.obs_state == ObsState.SCANNING:
                # Record scan and set status to ABORTED
                txn.eb_add_current_scan("ABORTED")
            txn.state_commanded = DevState.OFF
            txn.obs_state_commanded = ObsState.EMPTY
            if txn.eb_id is not None:
                LOG.info("Cancelling execution block %s", txn.eb_id)
                txn.cancel_eb()
            # Remove all resources
            txn.release_all_resources()

        self._update_attr_until_state([DevState.OFF])

    @command_transaction(argdesc="resource configuration")
    def AssignResources(self, transaction_id: str, config: str):
        """
        Assign resources to the subarray.

        This creates the execution block and the processing blocks.

        :param transaction_id: transaction ID
        :param config: JSON string containing resource configuration

        """
        for txn in self._config.txn():
            # Check if command is allowed
            command_allowed_state(txn.state, [DevState.ON])
            command_allowed_obs_state(
                txn.obs_state, [ObsState.EMPTY, ObsState.IDLE]
            )
            # If EB is in progress, cancel it
            if txn.eb_id is not None:
                LOG.info("Cancelling execution block %s", txn.eb_id)
                txn.cancel_eb()
            # Validate and parse the configuration
            resources, eblock, pbs = validate_assign_resources(txn, config)
            # Execute command
            txn.command = "AssignResources"
            txn.transaction_id = transaction_id
            txn.obs_state_commanded = ObsState.IDLE
            if resources:
                txn.assign_resources(resources)
            if eblock:
                txn.create_eb(eblock)
            if pbs:
                txn.create_pbs(pbs)

        self._update_attr_until_obs_state([ObsState.RESOURCING, ObsState.IDLE])

    @command_transaction(argdesc="resource configuration")
    def ReleaseResources(self, transaction_id: str, config: str):
        """
        Release all resources assigned to the subarray.

        This command is rejected if an execution block is assigned.

        :param transaction_id: transaction ID
        :param config: JSON string containing resource configuration

        """
        for txn in self._config.txn():
            # Check if command is allowed
            command_allowed_state(txn.state, [DevState.ON])
            command_allowed_obs_state(txn.obs_state, [ObsState.IDLE])
            # If EB is in progress, cancel it
            if txn.eb_id is not None:
                LOG.info("Cancelling execution block %s", txn.eb_id)
                txn.cancel_eb()
            # Validate and parse the configuration
            resources = validate_release_resources(config)
            # Execute command
            txn.command = "ReleaseResources"
            txn.transaction_id = transaction_id
            txn.release_resources(resources)
            if txn.resources == {}:
                obs_state_target = ObsState.EMPTY
            else:
                obs_state_target = ObsState.IDLE
            txn.obs_state_commanded = obs_state_target

        self._update_attr_until_obs_state([obs_state_target])

    @command_transaction()
    def ReleaseAllResources(self, transaction_id: str):
        """
        Release all resources assigned to the subarray.

        This command is rejected if an execution block is assigned.

        :param transaction_id: transaction ID

        """
        for txn in self._config.txn():
            # Check if command is allowed
            command_allowed_state(txn.state, [DevState.ON])
            command_allowed_obs_state(txn.obs_state, [ObsState.IDLE])
            # If EB is in progress, cancel it
            if txn.eb_id is not None:
                LOG.info("Cancelling execution block %s", txn.eb_id)
                txn.cancel_eb()
            # Execute command
            txn.command = "ReleaseAllResources"
            txn.transaction_id = transaction_id
            txn.obs_state_commanded = ObsState.EMPTY
            txn.release_all_resources()

        self._update_attr_until_obs_state([ObsState.EMPTY])

    @command_transaction(argdesc="scan type configuration")
    def Configure(self, transaction_id: str, config: str):
        """
        Configure scan type.

        :param transaction_id: transaction ID
        :param config: JSON string containing scan type configuration

        """
        for txn in self._config.txn():
            # Check if command is allowed
            command_allowed_state(txn.state, [DevState.ON])
            command_allowed_obs_state(
                txn.obs_state, [ObsState.IDLE, ObsState.READY]
            )
            # Reject command if EB is not assigned
            if txn.eb_id is None:
                message = "Execution block is not assigned"
                raise_command_failed(message)
            # Validate and parse the configuration string
            new_scan_types, scan_type = validate_configure(config)
            # Reject command if new scan types are supplied
            if new_scan_types is not None:
                message = "Configuration with new_scan_types is not supported"
                raise_command_failed(message)
            # Execute the command
            txn.command = "Configure"
            txn.transaction_id = transaction_id
            txn.obs_state_commanded = ObsState.READY
            txn.eb_scan_type = scan_type

        self._update_attr_until_obs_state(
            [ObsState.CONFIGURING, ObsState.READY]
        )

    @command_transaction()
    def End(self, transaction_id: str):
        """
        End the execution block.

        :param transaction_id: transaction ID

        """
        for txn in self._config.txn():
            # Check if command is allowed
            command_allowed_state(txn.state, [DevState.ON])
            command_allowed_obs_state(
                txn.obs_state, [ObsState.IDLE, ObsState.READY]
            )
            # Execute command
            txn.command = "End"
            txn.transaction_id = transaction_id
            txn.obs_state_commanded = ObsState.IDLE
            txn.eb_scan_type = None
            txn.finish_eb()

        self._update_attr_until_obs_state([ObsState.IDLE])

    @command_transaction(argdesc="scan ID")
    def Scan(self, transaction_id: str, config: str):
        """
        Start scan.

        :param transaction_id: transaction ID
        :param config: JSON string containing scan ID

        """
        for txn in self._config.txn():
            # Check if command is allowed
            command_allowed_state(txn.state, [DevState.ON])
            command_allowed_obs_state(txn.obs_state, [ObsState.READY])
            # Validate and parse the configuration string
            scan_id = validate_scan(config)
            # Execute the command
            txn.command = "Scan"
            txn.transaction_id = transaction_id
            txn.obs_state_commanded = ObsState.SCANNING
            txn.eb_scan_id = scan_id

        self._update_attr_until_obs_state([ObsState.SCANNING])

    @command_transaction()
    def EndScan(self, transaction_id: str):
        """
        End scan.

        :param transaction_id: transaction ID

        """
        for txn in self._config.txn():
            # Check if command is allowed
            command_allowed_state(txn.state, [DevState.ON])
            command_allowed_obs_state(txn.obs_state, [ObsState.SCANNING])
            # Execute command
            txn.command = "EndScan"
            txn.transaction_id = transaction_id
            # Record scan and set status to FINISHED
            txn.eb_add_current_scan("FINISHED")
            txn.obs_state_commanded = ObsState.READY
            txn.eb_scan_id = None

        self._update_attr_until_obs_state([ObsState.READY])

    @command_transaction()
    def Abort(self, transaction_id: str):
        """
        Abort the current activity.

        :param transaction_id: transaction ID

        """
        for txn in self._config.txn():
            # Check if command is allowed
            command_allowed_state(txn.state, [DevState.ON])
            command_allowed_obs_state(
                txn.obs_state,
                [
                    ObsState.RESOURCING,
                    ObsState.IDLE,
                    ObsState.CONFIGURING,
                    ObsState.READY,
                    ObsState.SCANNING,
                ],
            )
            # Execute command
            txn.command = "Abort"
            txn.transaction_id = transaction_id
            if txn.obs_state == ObsState.SCANNING:
                # Record scan and set status to ABORTED
                txn.eb_add_current_scan("ABORTED")
            txn.obs_state_commanded = ObsState.ABORTED

        self._update_attr_until_obs_state([ObsState.ABORTED])

    @command_transaction()
    def Restart(self, transaction_id: str):
        """
        Restart the subarray in the EMPTY obsState.

        :param transaction_id: transaction ID

        """
        for txn in self._config.txn():
            # Check if command is allowed
            command_allowed_state(txn.state, [DevState.ON])
            command_allowed_obs_state(
                txn.obs_state, [ObsState.ABORTED, ObsState.FAULT]
            )
            # Execute command
            txn.command = "Restart"
            txn.transaction_id = transaction_id
            txn.obs_state_commanded = ObsState.EMPTY
            txn.eb_scan_type = None
            txn.eb_scan_id = None
            if txn.eb_id is not None:
                LOG.info("Cancelling execution block %s", txn.eb_id)
                txn.cancel_eb()
            txn.release_all_resources()

        self._update_attr_until_obs_state([ObsState.EMPTY])

    # -------------------------
    # Attribute-setting methods
    # -------------------------

    def _set_obs_state(self, value):
        """Set obsState and push change and archive events."""
        old_value = None if self._obs_state is None else self._obs_state.name
        LOG.debug("Called _set_obs_state %s -> %s", old_value, value.name)
        if self._obs_state != value:
            LOG.info("Setting obsState to %s", value.name)
            self._obs_state = value
            self.push_change_event("obsState", self._obs_state)
            self.push_archive_event("obsState", self._obs_state)

    def _set_resources(self, value):
        """Set resources and push change and archive events."""
        value_str = json.dumps(value)
        if self._resources != value_str:
            if value:
                LOG.info("Setting resources")
            else:
                LOG.info("Clearing resources")
            self._resources = value_str
            self.push_change_event("resources", value_str)
            self.push_archive_event("resources", value_str)

    def _set_eb_id(self, value):
        """Set ebID and push change and archive events."""
        if value is None:
            value = "null"
        if self._eb_id != value:
            LOG.info("setting ebID to %s", value)
            self._eb_id = value
            self.push_change_event("ebID", self._eb_id)
            self.push_archive_event("ebID", self._eb_id)

    def _set_receive_addresses(self, value):
        """Set receiveAddresses and push change and archive events."""
        value_str = json.dumps(value)
        if self._receive_addresses != value_str:
            if value is None:
                LOG.info("Clearing receiveAddresses")
            else:
                LOG.info("Setting receiveAddresses")
            self._receive_addresses = value_str
            self.push_change_event("receiveAddresses", value_str)
            self.push_archive_event("receiveAddresses", value_str)

    def _set_scan_type(self, value):
        """Set scanType and push change and archive events."""
        if value is None:
            value = "null"
        if self._scan_type != value:
            LOG.info("Setting scanType to %s", value)
            self._scan_type = value
            self.push_change_event("scanType", self._scan_type)
            self.push_archive_event("scanType", self._scan_type)

    def _set_scan_id(self, value):
        """Set scanID and push change and archive events."""
        if value is None:
            value = 0
        if self._scan_id != value:
            LOG.info("Setting scanID to %d", value)
            self._scan_id = value
            self.push_change_event("scanID", self._scan_id)
            self.push_archive_event("scanID", self._scan_id)

    def _set_error_messages(self, value):
        """Set errorMessages and push change and archive events."""
        value_str = json.dumps(value)
        if self._error_messages != value_str:
            if value is None:
                LOG.info("Clearing errorMessages")
            else:
                LOG.info("Setting errorMessages")
            self._error_messages = value_str
            self.push_change_event("errorMessages", value_str)
            self.push_archive_event("errorMessages", value_str)

    # ------------------
    # Event loop methods
    # ------------------

    def _set_attributes(self, txn: SubarrayState) -> None:
        """
        Set attributes from configuration.

        This is called by the event loop.

        :param txn: configuration transaction

        """
        if txn.state_commanded is None:
            LOG.info("Cannot read subarray state: attributes cannot be set")
            return

        with log_transaction_id(txn.transaction_id):
            # Set attributes
            self._set_state(txn.state)
            self._set_admin_mode(txn.admin_mode)
            self._set_health_state(txn.health_state)
            self._set_obs_state(txn.obs_state)
            self._set_resources(txn.resources)
            self._set_eb_id(txn.eb_id)
            self._set_receive_addresses(txn.eb_receive_addresses)
            self._set_scan_type(txn.eb_scan_type)
            self._set_scan_id(txn.eb_scan_id)
            self._set_error_messages(txn.eb_error_messages)

    def _update_attr_until_obs_state(self, values):
        """
        Update attributes until obsState reaches one of the values.

        This is used as a substitute event loop inside a command.

        :param values: list of obsState values

        """
        self._update_attr_until_condition(lambda: self._obs_state in values)

    # ---------------
    # Utility methods
    # ---------------


def main(args=None, **kwargs):
    """Run server."""
    configure_root_logger()
    return SDPSubarray.run_server(
        args=args,
        post_init_callback=(devices_initialised, [SDPSubarray], {}),
        **kwargs,
    )
