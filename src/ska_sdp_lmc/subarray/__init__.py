"""SDP Subarray."""

from ska_sdp_lmc.subarray.device import SDPSubarray, main

__all__ = [
    "SDPSubarray",
    "main",
]
