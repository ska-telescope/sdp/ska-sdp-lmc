"""SKA SDP local monitoring and control (Tango devices)."""

from . import release
from .controller import SDPController
from .subarray import SDPSubarray

__version__ = release.VERSION

__all__ = [
    "__version__",
    "SDPController",
    "SDPSubarray",
]
