"""Test subarray device."""

# pylint: disable=protected-access
# pylint: disable=too-many-arguments
# pylint: disable=too-many-lines
# pylint: disable=too-many-locals
# pylint: disable=too-many-statements
# pylint: disable=too-many-positional-arguments

import json
import os
import time

import pytest
from device_utils import (
    EventMonitor,
    init_device,
    log_contains_device_name,
    log_contains_transaction_id,
    update_attributes,
    wait_for,
    wait_for_admin_mode,
    wait_for_health_state,
    wait_for_state,
)
from ska_control_model import AdminMode, HealthState, ObsState
from ska_sdp_config import ConfigCollision
from ska_sdp_config.entity import Script
from ska_telmodel.schema import validate
from ska_telmodel.sdp import SDP_RECVADDRS_PREFIX
from tango import DevFailed, DevState, DevString, DevVoid
from test_controller_device import set_owner

from ska_sdp_lmc import config, device
from ska_sdp_lmc.subarray import config as subarray_config
from ska_sdp_lmc.subarray import validation

DEVICE_NAME = "test-sdp/subarray/01"
COMPONENT_NAME = "lmc-subarray-01"
SUBARRAY_ID = "01"
SCHEMA_VERSION = "0.4"
RECEIVE_SCRIPTS = ["vis-receive"]

device.LOOP_INTERVAL = 0.5
subarray_config.ASSIGN_RESOURCES_TIMEOUT = 1.0
subarray_config.CONFIGURE_TIMEOUT = 1.0

# Observing states
OBS_STATE = [
    ObsState.EMPTY,
    ObsState.RESOURCING,
    ObsState.IDLE,
    ObsState.CONFIGURING,
    ObsState.READY,
    ObsState.SCANNING,
    ObsState.ABORTING,
    ObsState.ABORTED,
    ObsState.RESTARTING,
    ObsState.FAULT,
]

# Commands to change observing state and the observing states in which they are
# allowed
COMMAND_OBS_STATE = {
    "AssignResources": [ObsState.EMPTY, ObsState.IDLE],
    "ReleaseResources": [ObsState.IDLE],
    "ReleaseAllResources": [ObsState.IDLE],
    "Configure": [ObsState.IDLE, ObsState.READY],
    "End": [ObsState.IDLE, ObsState.READY],
    "Scan": [ObsState.READY],
    "EndScan": [ObsState.SCANNING],
    "Abort": [
        ObsState.RESOURCING,
        ObsState.IDLE,
        ObsState.CONFIGURING,
        ObsState.READY,
        ObsState.SCANNING,
    ],
    "Restart": [ObsState.ABORTED, ObsState.FAULT],
}

# Commands with arguments and the schema versions they support
COMMAND_SCHEMA_VERSION = {
    "AssignResources": ["0.4", "0.5", "1.0"],
    "ReleaseResources": ["0.4", "0.5", "1.0"],
    "Configure": ["0.4", "0.5", "1.0"],
    "Scan": ["0.4", "0.5", "1.0"],
}


@pytest.fixture(name="config_db_client", scope="module")
def get_config_db_client():
    """Config DB client instance used throughout these tests."""
    with config.new_config_db_client(COMPONENT_NAME) as cfg:
        yield cfg


@pytest.fixture(autouse=True)
def ensure_components_online(config_db_client):
    """
    Set processing controller and helm deployer status to ONLINE
    """
    # Ensure components are online before each test
    set_component_status(
        config_db_client, proccontrol_online=True, helmdeploy_online=True
    )

    yield


@pytest.fixture(name="subarray_device")
def get_subarray_device(devices):
    """Subarray device."""
    return init_device(devices, DEVICE_NAME)


@pytest.mark.parametrize(
    "command, input_type, output_type",
    [(command, DevVoid, DevVoid) for command in ["On", "Off"]]
    + [
        (
            command,
            DevString if command in COMMAND_SCHEMA_VERSION else DevVoid,
            DevVoid,
        )
        for command in COMMAND_OBS_STATE
    ],
)
def test_commands(subarray_device, command, input_type, output_type):
    """Command is present and has correct input and output types."""
    assert command in subarray_device.get_command_list()
    command_config = subarray_device.get_command_config(command)
    assert command_config.in_type == input_type
    assert command_config.out_type == output_type


def test_initialisation(caplog, config_db_client, subarray_device):
    """Device is initialised in the correct state."""
    wipe_config_db(config_db_client)
    subarray_device.Init()
    update_attributes(subarray_device)
    assert wait_for_state(subarray_device, DevState.OFF)
    assert wait_for_obs_state(subarray_device, ObsState.EMPTY)
    assert subarray_device.adminMode == AdminMode.ONLINE
    assert log_contains_device_name(caplog, DEVICE_NAME)
    assert not log_contains_transaction_id(caplog)


@pytest.mark.parametrize(
    "admin_mode, disabled",
    [
        (AdminMode.ONLINE, False),
        (AdminMode.OFFLINE, True),
        (AdminMode.ENGINEERING, False),
        (AdminMode.NOT_FITTED, True),
        (AdminMode.RESERVED, True),
    ],
)
@pytest.mark.parametrize("commanded_state", [DevState.ON, DevState.OFF])
def test_set_admin_mode(
    config_db_client, subarray_device, commanded_state, admin_mode, disabled
):
    """Admin mode can be set and modifies state."""
    set_state(
        config_db_client, subarray_device, state_commanded=commanded_state
    )
    set_admin_mode(subarray_device, admin_mode)
    expected_state = DevState.DISABLE if disabled else commanded_state
    assert subarray_device.State() == expected_state


@pytest.mark.parametrize("command", ["Off", "On"] + list(COMMAND_OBS_STATE))
@pytest.mark.parametrize(
    "admin_mode",
    [AdminMode.OFFLINE, AdminMode.NOT_FITTED, AdminMode.RESERVED],
)
def test_command_rejected_admin_mode(subarray_device, admin_mode, command):
    """Commands are rejected when admin mode sets state to DISABLE."""
    config_data = get_command_argument(command)
    argument = json.dumps(config_data) if config_data else None
    set_admin_mode(subarray_device, admin_mode)
    command_raises_exception(
        subarray_device, command, argument, "API_CommandNotAllowed"
    )


@pytest.mark.parametrize("command", ["Off"] + list(COMMAND_OBS_STATE))
def test_command_rejected_state(config_db_client, subarray_device, command):
    """Commands are rejected when state is OFF."""
    config_data = get_command_argument(command)
    argument = json.dumps(config_data) if config_data else None
    set_state(config_db_client, subarray_device, state_commanded=DevState.OFF)
    command_raises_exception(
        subarray_device, command, argument, "API_CommandNotAllowed"
    )


def test_on_command(caplog, config_db_client, subarray_device):
    """On command succeeds."""
    set_state(config_db_client, subarray_device, state_commanded=DevState.OFF)
    call_command(subarray_device, "On")
    assert subarray_device.State() == DevState.ON
    assert subarray_device.obsState == ObsState.EMPTY
    assert log_contains_transaction_id(caplog)


@pytest.mark.parametrize("obs_state", OBS_STATE)
def test_off_command(caplog, config_db_client, subarray_device, obs_state):
    """Off command succeeds."""
    set_state(config_db_client, subarray_device, obs_state_commanded=obs_state)
    call_command(subarray_device, "Off")
    assert subarray_device.State() == DevState.OFF
    assert subarray_device.obsState == ObsState.EMPTY
    assert log_contains_transaction_id(caplog)


def test_assign_resources_assigns_resources(
    caplog, config_db_client, subarray_device
):
    """AssignResources assigns resources."""
    command = "AssignResources"
    config_data = get_command_argument(command)
    del config_data["execution_block"]
    del config_data["processing_blocks"]
    resources = config_data["resources"]
    set_state(
        config_db_client, subarray_device, obs_state_commanded=ObsState.EMPTY
    )
    obs_state = EventMonitor(subarray_device, "obsState")
    call_command(subarray_device, command, json.dumps(config_data))
    assert obs_state.next_value() == ObsState.RESOURCING
    assert json.loads(subarray_device.resources) == resources
    assert log_contains_transaction_id(caplog)
    update_attributes(subarray_device)
    assert obs_state.next_value() == ObsState.IDLE


def test_assign_resources_starts_eb(caplog, config_db_client, subarray_device):
    """AssignResources starts execution block."""
    command = "AssignResources"
    config_data = get_command_argument(command)
    del config_data["resources"]
    eb_id = config_data["execution_block"]["eb_id"]
    set_state(
        config_db_client, subarray_device, obs_state_commanded=ObsState.EMPTY
    )
    obs_state = EventMonitor(subarray_device, "obsState")
    call_command(subarray_device, command, json.dumps(config_data))
    assert obs_state.next_value() == ObsState.RESOURCING
    assert subarray_device.ebID == eb_id
    assert log_contains_transaction_id(caplog)


def test_assign_resources_cancels_eb(
    caplog, config_db_client, subarray_device
):
    """AssignResources cancels execution block."""
    command = "AssignResources"
    config_data = get_command_argument(command)
    del config_data["execution_block"]
    del config_data["processing_blocks"]
    set_state(
        config_db_client, subarray_device, obs_state_commanded=ObsState.EMPTY
    )
    obs_state = EventMonitor(subarray_device, "obsState")
    call_command(subarray_device, command, json.dumps(config_data))
    assert obs_state.next_value() == ObsState.RESOURCING
    assert subarray_device.ebID == "null"
    assert log_contains_transaction_id(caplog)
    update_attributes(subarray_device)
    assert obs_state.next_value() == ObsState.IDLE


@pytest.mark.parametrize(
    "proccontrol_online, helmdeploy_online",
    [(True, False), (False, True), (False, False)],
)
def test_assign_resources_reacts_to_offline_components(
    config_db_client, subarray_device, proccontrol_online, helmdeploy_online
):
    """
    AssignResources command fails if either the
    processing controller or helm deployer are offline.
    """
    command = "AssignResources"
    config_data = get_command_argument(command)
    set_state(
        config_db_client, subarray_device, obs_state_commanded=ObsState.EMPTY
    )
    set_component_status(
        config_db_client, proccontrol_online, helmdeploy_online
    )
    command_raises_exception(
        subarray_device, command, json.dumps(config_data), "API_CommandFailed"
    )


@pytest.mark.parametrize("kind", ["EB", "PB"])
def test_assign_resources_rejected_invalid_id(
    config_db_client, subarray_device, kind
):
    """AssignResources is rejected if an ID is invalid."""
    command = "AssignResources"
    config_data = get_command_argument(command)
    if kind == "EB":
        config_data["execution_block"]["eb_id"] = "eb-wrong-eb-id-00000"
    if kind == "PB":
        config_data["processing_blocks"][0]["pb_id"] = "pb-wrong-pb-id-00000"
    validation.FEATURE_STRICT_VALIDATION.set_default(False)
    set_state(
        config_db_client, subarray_device, obs_state_commanded=ObsState.EMPTY
    )
    command_raises_exception(
        subarray_device, command, json.dumps(config_data), "API_CommandFailed"
    )
    validation.FEATURE_STRICT_VALIDATION.set_default(True)


def test_assign_resources_rejected_bad_script(
    config_db_client, subarray_device
):
    """AssignResources is rejected if processing script is not defined."""
    command = "AssignResources"
    config_data = get_command_argument(command)
    bad_script = {"kind": "realtime", "name": "nothing", "version": "none"}
    config_data["processing_blocks"][0]["script"] = bad_script
    set_state(
        config_db_client, subarray_device, obs_state_commanded=ObsState.EMPTY
    )
    command_raises_exception(
        subarray_device, command, json.dumps(config_data), "API_CommandFailed"
    )


def test_assign_resources_test_pb_params(
    caplog, config_db_client, subarray_device
):
    """AssignResources passes when script schemas don't exist."""
    command = "AssignResources"
    config_data = get_command_argument(command)
    set_state(
        config_db_client, subarray_device, obs_state_commanded=ObsState.EMPTY
    )
    # Parameters provided, but no schema
    for i, _ in enumerate(config_data["processing_blocks"]):
        config_data["processing_blocks"][i]["parameters"] = {
            "length": "any-length"
        }
    obs_state = EventMonitor(subarray_device, "obsState")
    call_command(subarray_device, command, json.dumps(config_data))
    assert obs_state.next_value() == ObsState.RESOURCING
    assert log_contains_transaction_id(caplog)
    update_attributes(subarray_device)


def test_assign_resources_reject_pb_params(config_db_client, subarray_device):
    """AssignResources is not allowed if pb params not valid."""
    command = "AssignResources"
    config_data = get_command_argument(command)

    for i, pb in enumerate(config_data["processing_blocks"]):
        script_key = Script.Key(**pb["script"])
        if script_key.name == "test-realtime":
            schema = {
                "additionalProperties": False,
                "description": "test-realtime script parameters",
                "properties": {
                    "length": {
                        "default": 3600.0,
                        "description": "Length of time, in seconds, "
                        "for the processing to complete",
                        "title": "Time for the processing to complete",
                        "type": "number",
                    }
                },
                "title": "test-realtime",
                "type": "object",
            }
            script = Script(
                key=script_key,
                image=":",
                parameters=schema,
            )
            for txn in config_db_client.txn():
                if txn.script.get(script_key):
                    txn.script.update(script)
                else:
                    txn.script.create(script)

            config_data["processing_blocks"][i]["parameters"] = {
                "length": "any-length"
            }
    set_state(
        config_db_client, subarray_device, obs_state_commanded=ObsState.EMPTY
    )

    command_raises_exception(
        subarray_device,
        command,
        json.dumps(config_data),
        "API_CommandFailed",
    )


def test_assign_resources_allow_pb_params(
    caplog, config_db_client, subarray_device
):
    """AssignResources go ahead for valid pb params."""
    command = "AssignResources"
    config_data = get_command_argument(command)

    for i, pb in enumerate(config_data["processing_blocks"]):
        script_key = Script.Key(**pb["script"])
        if script_key.name == "test-realtime":
            schema = {
                "additionalProperties": False,
                "description": "test-realtime script parameters",
                "properties": {
                    "length": {
                        "default": 3600.0,
                        "description": "Length of time, in seconds, "
                        "for the processing to complete",
                        "title": "Time for the processing to complete",
                        "type": "number",
                    }
                },
                "title": "test-realtime",
                "type": "object",
            }
            script = Script(
                key=script_key,
                image=":",
                parameters=schema,
            )
            for txn in config_db_client.txn():
                if txn.script.get(script_key):
                    txn.script.update(script)
                else:
                    txn.script.create(script)

            config_data["processing_blocks"][i]["parameters"] = {"length": 100}
    set_state(
        config_db_client, subarray_device, obs_state_commanded=ObsState.EMPTY
    )
    obs_state = EventMonitor(subarray_device, "obsState")
    call_command(subarray_device, command, json.dumps(config_data))
    assert obs_state.next_value() == ObsState.RESOURCING
    assert log_contains_transaction_id(caplog)
    update_attributes(subarray_device)


def test_assign_resources_rejected_bad_scan_type(
    config_db_client, subarray_device
):
    """AssignResources is rejected if scan_type is badly named."""
    command = "AssignResources"
    config_data = get_command_argument(command)
    # bad because id starts with '.' even though the type also
    # has a 'derive_from' key
    bad_scan_type = [
        {"scan_type_id": ".scan", "derive_from": ".default", "beams": {}}
    ]
    config_data["execution_block"]["scan_types"].extend(bad_scan_type)
    validation.FEATURE_STRICT_VALIDATION.set_default(False)
    set_state(
        config_db_client, subarray_device, obs_state_commanded=ObsState.EMPTY
    )
    command_raises_exception(
        subarray_device, command, json.dumps(config_data), "API_CommandFailed"
    )
    validation.FEATURE_STRICT_VALIDATION.set_default(True)


def test_ready_pbs_idle(config_db_client, subarray_device):
    """Ready real-time processing blocks cause transition to IDLE."""
    receive_addresses_expected = get_receive_addresses()
    set_state(
        config_db_client,
        subarray_device,
        obs_state_commanded=ObsState.IDLE,
        obs_state_expected=ObsState.RESOURCING,
        command="AssignResources",
        eb_assigned=True,
    )
    obs_state = EventMonitor(subarray_device, "obsState")
    make_pbs_ready(config_db_client, receive_addresses_expected)
    update_attributes(subarray_device)
    update_attributes(subarray_device)
    assert obs_state.next_value() == ObsState.IDLE
    receive_addresses = json.loads(subarray_device.receiveAddresses)
    assert receive_addresses == receive_addresses_expected
    # Validate receive addresses against schema
    recvaddrs_schema = SDP_RECVADDRS_PREFIX + SCHEMA_VERSION
    validate(recvaddrs_schema, receive_addresses, 2)


def test_unready_pbs_fault(config_db_client, subarray_device):
    """Unready real-time processing blocks cause transition to FAULT."""
    set_state(
        config_db_client,
        subarray_device,
        obs_state_commanded=ObsState.IDLE,
        obs_state_expected=ObsState.RESOURCING,
        command="AssignResources",
        eb_assigned=True,
    )
    obs_state = EventMonitor(subarray_device, "obsState")
    time.sleep(2 * subarray_config.ASSIGN_RESOURCES_TIMEOUT)
    update_attributes(subarray_device)
    assert obs_state.next_value() == ObsState.FAULT


def test_pbs_failed_obs_state_fault_assignres(
    config_db_client, subarray_device
):
    """obsState is FAULT when a pb has FAILED during AssignResources."""

    pb_state_failed = {
        "status": "FAILED",
        "error_messages": ["Image pull error"],
    }

    command = "AssignResources"
    config_data = get_command_argument(command)
    pb_id = config_data["processing_blocks"][0]["pb_id"]

    set_state(
        config_db_client, subarray_device, obs_state_commanded=ObsState.EMPTY
    )

    obs_state = EventMonitor(subarray_device, "obsState")
    call_command(subarray_device, command, json.dumps(config_data))
    assert obs_state.next_value() == ObsState.RESOURCING

    for txn in config_db_client.txn():
        try:
            txn.processing_block.state(pb_id).create(pb_state_failed)
        except ConfigCollision:
            txn.processing_block.state(pb_id).update(pb_state_failed)

    update_attributes(subarray_device)
    assert obs_state.next_value() == ObsState.FAULT
    config_db_client.backend.delete("/pb", recursive=True, must_exist=False)


def test_pbs_failed_obs_state_fault_configure(
    config_db_client, subarray_device
):
    """obsState is FAULT when a pb has FAILED during Configure."""
    pb_state_failed = {
        "status": "FAILED",
        "error_messages": ["Image pull error"],
    }

    command = "Configure"
    config_data = get_command_argument(command)
    pb_id = "pb-test-20220823-00000"

    set_state(
        config_db_client,
        subarray_device,
        obs_state_commanded=ObsState.IDLE,
        eb_assigned=True,
        pbs_ready=True,
    )

    obs_state = EventMonitor(subarray_device, "obsState")
    call_command(subarray_device, command, json.dumps(config_data))
    assert obs_state.next_value() == ObsState.CONFIGURING

    for txn in config_db_client.txn():
        try:
            txn.processing_block.state(pb_id).create(pb_state_failed)
        except ConfigCollision:
            txn.processing_block.state(pb_id).update(pb_state_failed)

    update_attributes(subarray_device)
    assert obs_state.next_value() == ObsState.FAULT
    config_db_client.backend.delete("/pb", recursive=True, must_exist=False)


def test_configure_configures(caplog, config_db_client, subarray_device):
    """Configure sets obs state to configuring."""
    command = "Configure"
    config_data = get_command_argument(command)
    set_state(
        config_db_client,
        subarray_device,
        obs_state_commanded=ObsState.IDLE,
        eb_assigned=True,
        pbs_ready=True,
    )
    obs_state = EventMonitor(subarray_device, "obsState")
    call_command(subarray_device, command, json.dumps(config_data))
    assert obs_state.next_value() == ObsState.CONFIGURING
    assert log_contains_transaction_id(caplog)


def test_configure_rejected_without_eb(config_db_client, subarray_device):
    """Configure is rejected if an execution block is not assigned."""
    command = "Configure"
    config_data = get_command_argument(command)
    set_state(
        config_db_client,
        subarray_device,
        obs_state_commanded=ObsState.IDLE,
        eb_assigned=False,
    )
    command_raises_exception(
        subarray_device, command, json.dumps(config_data), "API_CommandFailed"
    )


def test_configure_rejected_with_new_scan_types(
    config_db_client, subarray_device
):
    """Configure is rejected with new scan types."""
    command = "Configure"
    config_data = get_command_argument(command, version="new_scan_types")
    set_state(
        config_db_client,
        subarray_device,
        obs_state_commanded=ObsState.IDLE,
        eb_assigned=True,
    )
    command_raises_exception(
        subarray_device, command, json.dumps(config_data), "API_CommandFailed"
    )


def test_ready_deployments_ready(config_db_client, subarray_device):
    """Ready deployments cause transition to READY."""
    set_state(
        config_db_client,
        subarray_device,
        obs_state_commanded=ObsState.READY,
        obs_state_expected=ObsState.CONFIGURING,
        command="Configure",
        deployments_ready=False,
    )
    obs_state = EventMonitor(subarray_device, "obsState")
    make_deployments_ready(config_db_client)
    update_attributes(subarray_device)
    update_attributes(subarray_device)
    assert obs_state.next_value() == ObsState.READY


def test_unready_deployments_fault(config_db_client, subarray_device):
    """Unready deployments cause transition to FAULT."""
    set_state(
        config_db_client,
        subarray_device,
        obs_state_commanded=ObsState.READY,
        obs_state_expected=ObsState.CONFIGURING,
        command="Configure",
        deployments_ready=False,
    )
    obs_state = EventMonitor(subarray_device, "obsState")
    time.sleep(2 * subarray_config.CONFIGURE_TIMEOUT)
    update_attributes(subarray_device)
    assert obs_state.next_value() == ObsState.FAULT


def test_scan_starts_scanning_sets_scan_id(
    caplog, config_db_client, subarray_device
):
    """Scan starts scanning and sets scan ID."""
    command = "Scan"
    config_data = get_command_argument(command)
    scan_id = config_data["scan_id"]
    set_state(
        config_db_client, subarray_device, obs_state_commanded=ObsState.READY
    )
    obs_state = EventMonitor(subarray_device, "obsState")
    call_command(subarray_device, command, json.dumps(config_data))
    assert obs_state.next_value() == ObsState.SCANNING
    assert subarray_device.scanID == scan_id
    assert log_contains_transaction_id(caplog)


def test_end_scan_clears_scan_id(caplog, config_db_client, subarray_device):
    """EndScan clears scan ID."""
    set_state(
        config_db_client,
        subarray_device,
        obs_state_commanded=ObsState.SCANNING,
    )
    obs_state = EventMonitor(subarray_device, "obsState")
    call_command(subarray_device, "EndScan")
    assert obs_state.next_value() == ObsState.READY
    assert subarray_device.scanID == 0
    assert log_contains_transaction_id(caplog)


def test_end_ends_eb(caplog, config_db_client, subarray_device):
    """End ends execution block."""
    set_state(
        config_db_client, subarray_device, obs_state_commanded=ObsState.READY
    )
    obs_state = EventMonitor(subarray_device, "obsState")
    call_command(subarray_device, "End")
    assert obs_state.next_value() == ObsState.IDLE
    assert subarray_device.scanType == "null"
    assert subarray_device.ebID == "null"
    assert json.loads(subarray_device.receiveAddresses) == {}
    assert log_contains_transaction_id(caplog)


def test_release_resources_releases_resources(
    caplog, config_db_client, subarray_device
):
    """ReleaseResources releases resources."""
    command = "ReleaseResources"
    config_data = get_command_argument(command)
    set_state(
        config_db_client,
        subarray_device,
        obs_state_commanded=ObsState.IDLE,
        eb_assigned=False,
    )
    obs_state = EventMonitor(subarray_device, "obsState")
    call_command(subarray_device, command, json.dumps(config_data))
    assert obs_state.next_value() == ObsState.RESOURCING
    assert json.loads(subarray_device.resources) == {}
    assert log_contains_transaction_id(caplog)
    update_attributes(subarray_device)
    assert obs_state.next_value() == ObsState.EMPTY


def test_release_resources_cancels_eb(
    caplog, config_db_client, subarray_device
):
    """ReleaseResources cancels execution block."""
    command = "ReleaseResources"
    config_data = get_command_argument(command)
    set_state(
        config_db_client,
        subarray_device,
        obs_state_commanded=ObsState.IDLE,
        eb_assigned=True,
    )
    obs_state = EventMonitor(subarray_device, "obsState")
    call_command(subarray_device, command, json.dumps(config_data))
    assert obs_state.next_value() == ObsState.RESOURCING
    assert subarray_device.ebID == "null"
    assert json.loads(subarray_device.resources) == {}
    assert log_contains_transaction_id(caplog)
    update_attributes(subarray_device)
    assert obs_state.next_value() == ObsState.EMPTY


def test_release_all_resources_releases_all_resources(
    caplog, config_db_client, subarray_device
):
    """ReleaseAllResources releases all resources."""
    set_state(
        config_db_client,
        subarray_device,
        obs_state_commanded=ObsState.IDLE,
        eb_assigned=False,
    )
    obs_state = EventMonitor(subarray_device, "obsState")
    call_command(subarray_device, "ReleaseAllResources")
    assert obs_state.next_value() == ObsState.RESOURCING
    assert json.loads(subarray_device.resources) == {}
    assert log_contains_transaction_id(caplog)
    update_attributes(subarray_device)
    assert obs_state.next_value() == ObsState.EMPTY


def test_release_all_resources_cancels_eb(
    caplog, config_db_client, subarray_device
):
    """ReleaseAllResources cancels execution block."""
    set_state(
        config_db_client,
        subarray_device,
        obs_state_commanded=ObsState.IDLE,
        eb_assigned=True,
    )
    obs_state = EventMonitor(subarray_device, "obsState")
    call_command(subarray_device, "ReleaseAllResources")
    assert obs_state.next_value() == ObsState.RESOURCING
    assert subarray_device.ebID == "null"
    assert json.loads(subarray_device.resources) == {}
    assert log_contains_transaction_id(caplog)
    update_attributes(subarray_device)
    assert obs_state.next_value() == ObsState.EMPTY


@pytest.mark.parametrize("obs_state", COMMAND_OBS_STATE["Abort"])
def test_abort_command(config_db_client, subarray_device, obs_state):
    """Abort command succeeds."""
    set_state(config_db_client, subarray_device, obs_state_commanded=obs_state)
    obs_state = EventMonitor(subarray_device, "obsState")
    call_command(subarray_device, "Abort")
    assert obs_state.next_value() == ObsState.ABORTING
    update_attributes(subarray_device)
    assert obs_state.next_value() == ObsState.ABORTED


@pytest.mark.parametrize("obs_state", COMMAND_OBS_STATE["Restart"])
def test_restart_command(config_db_client, subarray_device, obs_state):
    """Restart command succeeds."""
    set_state(config_db_client, subarray_device, obs_state_commanded=obs_state)
    obs_state = EventMonitor(subarray_device, "obsState")
    call_command(subarray_device, "Restart")
    assert obs_state.next_value() == ObsState.RESTARTING
    update_attributes(subarray_device)
    assert obs_state.next_value() == ObsState.EMPTY


@pytest.mark.parametrize(
    "command, obs_state",
    [
        (command, obs_state)
        for command, obs_states_allowed in COMMAND_OBS_STATE.items()
        for obs_state in OBS_STATE
        if obs_state not in obs_states_allowed
    ],
)
def test_command_rejected_obs_state(
    config_db_client, subarray_device, command, obs_state
):
    """Command is rejected in disallowed obsState."""
    set_state(config_db_client, subarray_device, obs_state_commanded=obs_state)
    config_data = get_command_argument(command)
    argument = json.dumps(config_data) if config_data else None
    command_raises_exception(
        subarray_device, command, argument, "API_CommandNotAllowed"
    )


@pytest.mark.parametrize(
    "command, obs_state",
    [
        ("AssignResources", ObsState.EMPTY),
        ("AssignResources", ObsState.IDLE),
        ("Configure", ObsState.IDLE),
        ("Configure", ObsState.READY),
        ("Scan", ObsState.READY),
    ],
)
def test_command_rejected_invalid_json(
    config_db_client, subarray_device, command, obs_state
):
    """Command is rejected with an invalid JSON configuration."""
    config_data = get_command_argument(command, version="invalid")
    eb_assigned = command != "AssignResources"
    ready = obs_state != ObsState.EMPTY
    set_state(
        config_db_client,
        subarray_device,
        obs_state_commanded=obs_state,
        eb_assigned=eb_assigned,
        pbs_ready=ready,
        deployments_ready=ready,
    )
    command_raises_exception(
        subarray_device, command, json.dumps(config_data), "API_CommandFailed"
    )
    assert subarray_device.obsState == obs_state


@pytest.mark.parametrize(
    "command, initial_obs_state, final_obs_state",
    [
        ("AssignResources", ObsState.EMPTY, ObsState.RESOURCING),
        ("AssignResources", ObsState.IDLE, ObsState.RESOURCING),
        ("Configure", ObsState.IDLE, ObsState.CONFIGURING),
        ("Configure", ObsState.READY, ObsState.CONFIGURING),
        ("Scan", ObsState.READY, ObsState.SCANNING),
    ],
)
@pytest.mark.parametrize("version", ["0.4", "0.5"])
# pylint: disable=too-many-positional-arguments
def test_command_accepted_old_schema(
    config_db_client,
    subarray_device,
    version,
    command,
    initial_obs_state,
    final_obs_state,
):
    """Command is accepted with old schema version."""
    config_data = get_command_argument(command, version="schema_0.4")
    config_data["interface"] = config_data["interface"].replace("0.4", version)
    eb_assigned = command != "AssignResources"
    ready = initial_obs_state != ObsState.EMPTY
    set_state(
        config_db_client,
        subarray_device,
        obs_state_commanded=initial_obs_state,
        eb_assigned=eb_assigned,
        pbs_ready=ready,
        deployments_ready=ready,
    )
    obs_state = EventMonitor(subarray_device, "obsState")
    call_command(subarray_device, command, json.dumps(config_data))
    assert obs_state.next_value() == final_obs_state


@pytest.mark.parametrize(
    "command, obs_state",
    [
        ("AssignResources", ObsState.EMPTY),
        ("AssignResources", ObsState.IDLE),
        ("Configure", ObsState.IDLE),
        ("Configure", ObsState.READY),
        ("Scan", ObsState.READY),
    ],
)
def test_command_rejected_without_interface(
    config_db_client,
    subarray_device,
    command,
    obs_state,
):
    """Command is rejected without interface value."""
    config_data = get_command_argument(command)
    del config_data["interface"]
    eb_assigned = command != "AssignResources"
    ready = obs_state != ObsState.EMPTY
    set_state(
        config_db_client,
        subarray_device,
        obs_state_commanded=obs_state,
        eb_assigned=eb_assigned,
        pbs_ready=ready,
        deployments_ready=ready,
    )
    command_raises_exception(
        subarray_device, command, json.dumps(config_data), "API_CommandFailed"
    )


@pytest.mark.parametrize(
    "proccontrol_online, helmdeploy_online, health_state",
    [
        (True, True, HealthState.OK),
        (False, True, HealthState.DEGRADED),
        (True, False, HealthState.DEGRADED),
        (False, False, HealthState.DEGRADED),
    ],
)
def test_components_health_state(
    config_db_client,
    subarray_device,
    proccontrol_online,
    helmdeploy_online,
    health_state,
):
    """Health state is correctly updated based on component status."""
    set_component_status(
        config_db_client, proccontrol_online, helmdeploy_online
    )
    update_attributes(subarray_device)
    assert wait_for_health_state(subarray_device, health_state)


def test_pb_with_error_messages_health_state(
    config_db_client, subarray_device
):
    """HealthState is DEGRADED when pbs are RUNNING with error messages."""
    pb_with_error = {
        "status": "RUNNING",
        "error_messages": ["Back-off pulling image"],
    }

    set_state(
        config_db_client,
        subarray_device,
        pb_status=[pb_with_error],
        obs_state_commanded=ObsState.READY,
        eb_assigned=True,
        command=None,
    )
    update_attributes(subarray_device)
    assert wait_for_health_state(subarray_device, HealthState.DEGRADED)


# Helper functions


def wait_for_state_and_obs_state(tango_device, state, obs_state):
    """
    Wait for device state and obsState to reach the required values.

    :param tango_device: tango device
    :param state: required state value
    :param obs_state: required obsState value

    """
    return wait_for(
        lambda: tango_device.state() == state
        and tango_device.obsState == obs_state
    )


def wait_for_obs_state(tango_device, obs_state):
    """
    Wait for obsState to reach the required value.

    :param tango_device: tango device
    :param obs_state: required obsState value
    """
    return wait_for(lambda: tango_device.obsState == obs_state)


def wipe_config_db(config_db_client):
    """Remove the subarray, EB and PB entries in the config DB."""
    config_db_client.backend.delete(
        "/component", recursive=True, must_exist=False
    )
    config_db_client.backend.delete("/eb", recursive=True, must_exist=False)
    config_db_client.backend.delete("/pb", recursive=True, must_exist=False)
    config_db_client.backend.delete(
        "/script", recursive=True, must_exist=False
    )


# pylint: disable=too-many-positional-arguments, too-many-branches
def set_state(
    config_db_client,
    subarray_device,
    state_commanded=DevState.ON,
    obs_state_commanded=ObsState.EMPTY,
    obs_state_expected=None,
    command=None,
    eb_assigned=False,
    pbs_ready=False,
    deployments_ready=False,
    pb_status=None,
):
    """
    Set subarray internal state.

    :param state_commanded: Commanded device state
    :param obs_state_commanded: Commanded obsState
    :param obs_state_expected: Expected obsState (if None, assumed to be
        obs_state_commanded)
    :param command: Command
    :param eb_assigned: Execution block and processing blocks are assigned
    :param pbs_ready: Real-time processing blocks are ready
    :param deployments_ready: Processing deployments are ready
    """
    # Read configuration data

    scripts = get_scripts()

    # Some argument values are overridden here, depending on the commanded
    # obsState
    if obs_state_commanded == ObsState.EMPTY:
        resources = {}
        eblock, pbs = None, None
        pbs_ready = False
        receive_addresses = None
        deployments_ready = False
        scan_type = None
        scan_id = None
    elif obs_state_commanded in (ObsState.IDLE, ObsState.ABORTED):
        resources = get_resources()
        eblock, pbs = get_eb_pbs() if eb_assigned else (None, None)
        # pbs_ready as specified in arguments
        receive_addresses = get_receive_addresses() if pbs_ready else None
        # deployments_ready as specified in arguments
        scan_type = None
        scan_id = None
    elif obs_state_commanded == ObsState.READY:
        resources = get_resources()
        eblock, pbs = get_eb_pbs()
        pbs_ready = True
        receive_addresses = get_receive_addresses()
        # deployments_ready as specified in arguments
        scan_type = get_scan_type()
        scan_id = None
    elif obs_state_commanded == ObsState.SCANNING:
        resources = get_resources()
        eblock, pbs = get_eb_pbs()
        pbs_ready = True
        receive_addresses = get_receive_addresses()
        deployments_ready = True
        scan_type = get_scan_type()
        scan_id = get_scan_id()
    else:
        resources = None
        eblock, pbs = None, None
        pbs_ready = False
        receive_addresses = None
        deployments_ready = False
        scan_type = None
        scan_id = None

    for txn in config_db_client.txn():
        # Create processing script entries, if they don't exist

        for key in scripts:
            script = Script(key=Script.Key(**key), image=":")
            if txn.script.get(script) is None:
                txn.script.create(script)

        # Delete old EB and PB entries

        for eb_id in txn.execution_block.query_keys():
            txn.raw.delete("/eb/" + eb_id)
            txn.raw.delete("/eb/" + eb_id + "/state", must_exist=False)
        for pb_id in txn.processing_block.query_keys():
            txn.raw.delete("/pb/" + pb_id)
            txn.raw.delete("/pb/" + pb_id + "/state", must_exist=False)

        # Update subarray entry

        subarray = subarray_config.SubarrayState(txn, SUBARRAY_ID)
        subarray.transaction_id = None
        subarray.command = command
        subarray.state_commanded = state_commanded
        subarray.obs_state_commanded = obs_state_commanded
        subarray.admin_mode = AdminMode.ONLINE
        subarray._set("resources", resources)
        subarray._set("eb_id", None)

        if eblock:
            subarray.create_eb(eblock)
            subarray.create_pbs(pbs)
            create_pb_states(
                subarray, pbs_ready, receive_addresses, deployments_ready
            )
            pb_realtime = subarray._get_eb("pb_realtime", default=[])

            if pb_status is not None:
                for pb_id, status in zip(pb_realtime, pb_status):
                    subarray._txn.processing_block.state(pb_id).update(status)

        subarray.eb_scan_type = scan_type
        subarray.eb_scan_id = scan_id

    update_attributes(subarray_device)

    if obs_state_expected is None:
        obs_state_expected = obs_state_commanded

    assert wait_for_state_and_obs_state(
        subarray_device, state_commanded, obs_state_expected
    )


def create_pb_states(
    subarray, pbs_ready, receive_addresses, deployments_ready
):
    """
    Create processing block states.

    :param subarray: instance of subarray config interface
    :param pbs_ready: are real-time processing blocks ready?
    :param receive_addresses: receive addresses to set
    :param deployments_ready: are processing deployments ready?
    """
    for pb_id in subarray._get_eb("pb_realtime", default=[]):
        status = "READY" if pbs_ready else "RUNNING"
        state = {"status": status}
        if receive_addresses is not None:
            pblock = subarray._txn.processing_block.get(pb_id)
            if pblock.script.name in RECEIVE_SCRIPTS:
                state["receive_addresses"] = receive_addresses
        if deployments_ready:
            state["deployments_ready"] = True
        subarray._txn.processing_block.state(pb_id).create(state)

    for pb_id in subarray._get_eb("pb_batch", default=[]):
        state = {"status": "RUNNING"}
        subarray._txn.processing_block.state(pb_id).create(state)


def set_admin_mode(subarray_device, admin_mode):
    """Set the adminMode to the specified value."""
    assert admin_mode in AdminMode
    subarray_device.adminMode = admin_mode
    update_attributes(subarray_device)
    assert wait_for_admin_mode(subarray_device, admin_mode)


def call_command(subarray_device, command, argument=None):
    """Call command."""
    assert command in subarray_device.get_command_list()
    subarray_device.command_inout(command, argument)


def command_raises_exception(subarray_device, command, argument, reason):
    """Call command and check that it raises exception."""
    assert command in subarray_device.get_command_list()
    with pytest.raises(DevFailed) as exception:
        subarray_device.command_inout(command, argument)
    assert exception.value.args[0].reason == reason


def make_pbs_ready(config_db_client, receive_addresses):
    """Make real-time processing blocks ready."""
    for txn in config_db_client.txn():
        subarray = subarray_config.SubarrayState(txn, SUBARRAY_ID)
        pb_realtime = subarray._get_eb("pb_realtime", default=[])
        for pb_id in pb_realtime:
            pblock = subarray._txn.processing_block.get(pb_id)
            pb_state = subarray._txn.processing_block.state(pb_id).get()
            pb_state["status"] = "READY"
            if pblock.script.name in RECEIVE_SCRIPTS:
                pb_state["receive_addresses"] = receive_addresses
            subarray._txn.processing_block.state(pb_id).update(pb_state)


def make_deployments_ready(config_db_client):
    """Make real-time processing deployments ready."""
    for txn in config_db_client.txn():
        subarray = subarray_config.SubarrayState(txn, SUBARRAY_ID)
        pb_realtime = subarray._get_eb("pb_realtime", default=[])
        for pb_id in pb_realtime:
            pb_state = subarray._txn.processing_block.state(pb_id).get()
            pb_state["deployments_ready"] = True
            subarray._txn.processing_block.state(pb_id).update(pb_state)


def set_component_status(
    config_db_client, proccontrol_online, helmdeploy_online
):
    """Create/delete owner entries of components."""
    for txn in config_db_client.txn():
        set_owner(txn, "proccontrol", proccontrol_online)
        set_owner(txn, "helmdeploy", helmdeploy_online)


def get_resources():
    """Get resources from AssignResources argument."""
    config_data = get_command_argument("AssignResources")
    resources = config_data.get("resources")
    return resources


def get_eb_pbs():
    """Get EB and PBs from AssignResources argument."""
    config_data = get_command_argument("AssignResources")
    eblock = config_data.get("execution_block")
    pbs = config_data.get("processing_blocks")
    return eblock, pbs


def get_scripts():
    """Get processing scripts from AssignResources argument."""
    config_data = get_command_argument("AssignResources")
    pbs = config_data.get("processing_blocks")
    return list(pb.get("script") for pb in pbs)


def get_scan_type():
    """Get scan type from Configure argument."""
    config_data = get_command_argument("Configure")
    scan_type = config_data.get("scan_type")
    return scan_type


def get_scan_id():
    """Get scan ID from Scan argument."""
    config_data = get_command_argument("Scan")
    scan_id = config_data.get("scan_id")
    return scan_id


def get_command_argument(name, version=None):
    """
    Get command argument from JSON file.

    :param name: name of command
    :param version: version suffix for filename
    """
    if version:
        filename = f"command_{name}_{version}.json"
    else:
        filename = f"command_{name}.json"
    return read_json_data(filename)


def get_receive_addresses():
    """Get receive addresses from JSON file."""
    return read_json_data("receive_addresses.json")


def read_json_data(filename):
    """
    Read JSON file from data directory.

    If the file does not exist, it returns None.
    """
    path = os.path.join(os.path.dirname(__file__), "data", filename)
    if os.path.exists(path):
        with open(path, "r", encoding="utf-8") as file:
            data = json.load(file)
    else:
        data = None
    return data
