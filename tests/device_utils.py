"""Utilities for device tests."""

import logging
import queue
import time
from typing import Callable

import pytest
from ska_control_model import AdminMode, HealthState
from tango import DeviceProxy, DevState, EventType
from tango.test_context import MultiDeviceTestContext

from ska_sdp_lmc import device as device_mod

LOG = logging.getLogger(__name__)

SLEEP = 0.05
TIMEOUT = 5.0


def init_device(devices: MultiDeviceTestContext, name: str) -> DeviceProxy:
    """Initialise a device."""
    device = devices.get_device(name)

    # Update the device attributes if the event loop is not running
    update_attributes(device)

    return device


def update_attributes(device: DeviceProxy) -> None:
    """Update attribute if event loop is not running."""
    if not device_mod.FEATURE_EVENT_LOOP.is_active():
        device.update_attributes()


def wait_for(
    predicate: Callable[[], bool],
    timeout: float = TIMEOUT,
    sleep: float = SLEEP,
) -> bool:
    """Wait for predicate to be true."""
    elapsed = 0.0
    while not predicate() and elapsed < timeout:
        time.sleep(sleep)
        elapsed += sleep
    if elapsed >= timeout:
        LOG.warning("Timeout occurred while waiting")
        return False
    return True


def wait_for_state(device: DeviceProxy, state: DevState) -> bool:
    """Wait for device state to reach the required value."""
    return wait_for(lambda: device.state() == state)


def wait_for_admin_mode(device: DeviceProxy, admin_mode: AdminMode) -> bool:
    """Wait for device adminMode to reach the required value."""
    return wait_for(lambda: device.adminMode == admin_mode)


def wait_for_health_state(
    device: DeviceProxy, health_state: HealthState
) -> bool:
    """Wait for device healthState to reach the required value."""
    return wait_for(lambda: device.healthState == health_state)


def log_contains_device_name(
    caplog: pytest.LogCaptureFixture, device_name: str
):
    """Log contains the device name."""

    device_tag = f"tango-device:{device_name}"

    for record in caplog.records:
        for tag in record.tags.split(","):
            if tag == device_tag:
                return True
    return False


def log_contains_transaction_id(caplog: pytest.LogCaptureFixture):
    """Log contains a transaction ID."""

    for record in caplog.records:
        for tag in record.tags.split(","):
            if tag.startswith("txn-"):
                return True
    return False


class EventMonitor:
    """
    Monitor events on Tango device attribute.

    :param device: device proxy
    :param attribute: name of attribute
    :param event_type: event type to subscribe to
    """

    def __init__(
        self,
        device: DeviceProxy,
        attribute: str,
        event_type: EventType = EventType.CHANGE_EVENT,
    ):
        self._queue = queue.SimpleQueue()
        sub_id = device.subscribe_event(
            attribute, event_type, self._queue.put_nowait
        )
        self._unsubscribe = lambda: device.unsubscribe_event(sub_id)
        # When the subscription is created, it sends an event with the current
        # value, so clear the queue before continuing
        self.clear()

    def __del__(self):
        self._unsubscribe()

    def clear(self):
        """Clear the queue."""
        while not self._queue.empty():
            self._queue.get_nowait()

    def next_value(self, timeout=TIMEOUT):
        """Get next attribute value from queue."""
        try:
            event = self._queue.get(timeout=timeout)
            return event.attr_value.value
        except queue.Empty:
            return None
