# add_script is used but not called in the functions:
# pylint: disable=unused-argument

"""Test subarray validation."""

import json
from pathlib import Path

import jsonschema
import pytest
from ska_sdp_config.entity import Script

from ska_sdp_lmc.subarray.validation import (
    _convert_assign_res_to_v1_0,
    script_compatible_with_sdp,
    validate_pb_params,
)

SCRIPT_KIND = "realtime"
SCRIPT_NAME = "test_realtime"
SCRIPT_VERSION = "0.0.1"


@pytest.fixture(scope="function", name="add_script")
def _load_script_to_db(subar_config):
    """
    Generate script definitions in the config DB.

    The productions schemas are generated and stored in the
    ska-sdp-script repository (tmdata). The example below
    is based on these.
    """

    script_image = {"image": f"car://repo/my-script:{SCRIPT_VERSION}"}
    script_schema = {
        "$defs": {
            "Env": {
                "description": "Environment variables",
                "properties": {
                    "name": {"type": "string"},
                    "value": {"type": "string"},
                },
                "required": ["name", "value"],
                "title": "Env",
                "type": "object",
            },
        },
        "additionalProperties": False,
        "description": "Script parameters",
        "properties": {
            "duration": {
                "default": 60.0,
                "description": "Duration",
                "title": "Duration of processing",
                "type": "number",
            },
            "env": {"items": {"$ref": "#/$defs/Env"}, "type": "array"},
        },
        "title": "test-batch",
        "type": "object",
    }

    script_key = Script.Key(
        kind=SCRIPT_KIND, name=SCRIPT_NAME, version=SCRIPT_VERSION
    )
    script = Script(key=script_key, **script_image, parameters=script_schema)

    # Create script
    for txn in subar_config.txn():
        # pylint: disable-next=protected-access
        txn._txn.script.create(script)


@pytest.mark.parametrize(
    "sdp_version, script_sdp_version, compatible",
    [
        (None, None, None),
        (None, ">=1.2.3", None),
        ("", ">=1.2.3", None),
        ("1.2.3", None, None),
        ("1.2.3", "", None),
        ("1.2.3", ">=1.2.3", True),
        ("1.2.3-rc.1", ">=1.2.3", True),
        ("1.2.4", ">=1.2.3", True),
        ("1.2.2", ">=1.2.3", False),
    ],
)
def test_script_compatible_with_sdp(
    sdp_version, script_sdp_version, compatible
):
    """
    Test script_compatible_with_sdp function.
    """
    assert (
        script_compatible_with_sdp(sdp_version, script_sdp_version)
        == compatible
    )


def test_convert_assignres_to_1_0():
    """
    Test v0.4 schema can be converted to v1.0 schema
    """
    path_0_4 = Path("tests/data/command_AssignResources_schema_0.4.json")
    config_0_4 = json.loads(path_0_4.read_text(encoding="utf-8"))

    converted_config = _convert_assign_res_to_v1_0(config_0_4)

    path_0_1 = Path("tests/data/command_AssignResources.json")
    config_0_1 = json.loads(path_0_1.read_text(encoding="utf-8"))

    assert converted_config == config_0_1


def test_pb_schema_validation(subar_config, add_script):
    """Validation passes when pb parameters match basic schema."""
    pb = {
        "pb_id": "pb-test-19870901-999999",
        "script": {
            "kind": SCRIPT_KIND,
            "name": SCRIPT_NAME,
            "version": SCRIPT_VERSION,
        },
        "parameters": {"duration": 14.0},
    }

    for txn in subar_config.txn():
        res = validate_pb_params(txn, pb)

        assert res is None


def test_pb_schema_validation_additional_properties(subar_config, add_script):
    """
    Validation fails and returns the error, when additional,
    unexpected, properties are added to pb parameters.
    """
    pb = {
        "pb_id": "pb-test-19870901-999999",
        "script": {
            "kind": SCRIPT_KIND,
            "name": SCRIPT_NAME,
            "version": SCRIPT_VERSION,
        },
        "parameters": {"duration": 14.0, "unexpected-arg": []},
    }

    for txn in subar_config.txn():
        res = validate_pb_params(txn, pb)

        assert isinstance(res, jsonschema.exceptions.ValidationError)
        assert (
            res.message == "Additional properties are not allowed "
            "('unexpected-arg' was unexpected)"
        )


def test_pb_schema_validation_extra_defs(subar_config, add_script):
    """
    Validation passes when pb parameters contain properties
    which appear under Defs in the schema definition.
    """
    pb = {
        "pb_id": "pb-test-19870901-999999",
        "script": {
            "kind": SCRIPT_KIND,
            "name": SCRIPT_NAME,
            "version": SCRIPT_VERSION,
        },
        "parameters": {
            "duration": 14.0,
            "env": [{"name": "EnvVar", "value": "Test"}],
        },
    }

    for txn in subar_config.txn():
        res = validate_pb_params(txn, pb)

        assert res is None
