"""Test subarray configuration interface."""

# pylint: disable=protected-access

import pytest
from ska_control_model import AdminMode, HealthState, ObsState
from ska_sdp_config.entity import Script
from tango import DevFailed, DevState
from test_controller_config import component_online

SUBARRAY_ID = "01"

SCRIPT_KEY = {"kind": "realtime", "name": "test", "version": "0.1.0"}
SCRIPT_IMAGE = "ska-sdp-script-test:0.1.0"
SCRIPT = Script(
    key=Script.Key(**SCRIPT_KEY), image=SCRIPT_IMAGE, sdp_version=None
)


def test_initialisation(subar_config):
    """
    Test subarray device configuration interface.
    Device is initialised correctly.
    """
    # Initialisation
    for txn in subar_config.txn():
        txn.create_if_not_present(
            DevState.OFF, ObsState.EMPTY, AdminMode.ONLINE
        )

    for txn in subar_config.txn():
        assert txn.command is None
        assert txn.transaction_id is None
        assert txn.state_commanded == DevState.OFF
        assert txn.state == DevState.OFF
        assert txn.obs_state_commanded == ObsState.EMPTY
        assert txn.obs_state == ObsState.EMPTY
        assert txn.eb_scan_type is None
        assert txn.eb_scan_id is None
        assert txn.eb_receive_addresses == {}


@pytest.mark.parametrize(
    "admin_mode,disabled",
    [
        (AdminMode.ONLINE, False),
        (AdminMode.OFFLINE, True),
        (AdminMode.ENGINEERING, False),
        (AdminMode.RESERVED, True),
        (AdminMode.NOT_FITTED, True),
    ],
)
@pytest.mark.parametrize("state", [DevState.ON, DevState.OFF])
def test_admin_mode(subar_config, state, admin_mode, disabled):
    """
    When admin_mode is set to OFFLINE, NOT_FITTED or RESERVED then the state is
    reported as DISABLE, otherwise it is the commanded state.
    """
    # Initialisation
    for txn in subar_config.txn():
        txn.create_if_not_present(state, ObsState.EMPTY, admin_mode)

    state_expected = DevState.DISABLE if disabled else state
    for txn in subar_config.txn():
        assert txn.state == state_expected


def test_update_attributes(subar_config):
    """
    Test subarray device configuration interface.
    Attributes are updated correctly.
    """
    # Initialisation
    for txn in subar_config.txn():
        txn.create_if_not_present(
            DevState.OFF, ObsState.EMPTY, AdminMode.ONLINE
        )

    # Set attributes
    for txn in subar_config.txn():
        txn.command = "command_aaa"
        txn.transaction_id = "txn-aaa"
        txn.state_commanded = DevState.ON
        txn.obs_state_commanded = ObsState.IDLE
        txn.admin_mode = AdminMode.ENGINEERING
        # scan_type and scan_id should be ignored, because no EB is configured
        txn.eb_scan_type = "xxx"
        txn.eb_scan_id = 123

    for txn in subar_config.txn():
        assert txn.command == "command_aaa"
        assert txn.transaction_id == "txn-aaa"
        assert txn.state_commanded == DevState.ON
        assert txn.state == DevState.ON
        assert txn.admin_mode == AdminMode.ENGINEERING
        assert txn.obs_state_commanded == ObsState.IDLE
        assert txn.obs_state == ObsState.IDLE
        assert txn.eb_scan_type is None
        assert txn.eb_scan_id is None
        assert txn.eb_receive_addresses == {}
        assert txn.eb_error_messages == {}


def test_one_eb(subar_config):
    """
    Test subarray device configuration interface.
    Subarray works with execution blocks and
    their processing blocks. Create one EB.
    """
    # Initialisation
    for txn in subar_config.txn():
        txn._txn.script.create(SCRIPT)
        txn.create_if_not_present(
            DevState.OFF, ObsState.EMPTY, AdminMode.ONLINE
        )

    # Set the processing controller and helm deployer to be online
    for txn in subar_config.txn():
        component_online(txn, "proccontrol", True)
        component_online(txn, "helmdeploy", True)

    # Create first EB and its PBs
    eb_id = "eb-xxx-20240413-00000"
    pb_id = "pb-xxx-20240413-00000"
    for txn in subar_config.txn():
        eblock = fake_eb(eb_id)
        pblock = fake_pb(pb_id)
        txn.create_eb(eblock)
        txn.create_pbs([pblock])

    for txn in subar_config.txn():
        assert txn.eb_scan_type is None
        assert txn.eb_scan_id is None
        assert txn.eb_receive_addresses == {}
        # Use low-level EB interface to check EB
        eblock = txn._txn.execution_block.get(eb_id)
        assert eblock.subarray_id == SUBARRAY_ID
        eb_state = txn._txn.execution_block.state(eb_id).get()
        assert eb_state["status"] == "ACTIVE"

    # Select scan type and set scan ID
    for txn in subar_config.txn():
        txn.eb_scan_type = "xxx"
        txn.eb_scan_id = 456

    for txn in subar_config.txn():
        assert txn.eb_scan_type == "xxx"
        assert txn.eb_scan_id == 456

    # Finish EB
    for txn in subar_config.txn():
        txn.finish_eb()

    for txn in subar_config.txn():
        assert txn.eb_scan_type is None
        assert txn.eb_scan_id is None
        assert txn.eb_receive_addresses == {}
        # Use low-level EB interface to check EB
        eblock = txn._txn.execution_block.get(eb_id)
        assert eblock.subarray_id is None
        eb_state = txn._txn.execution_block.state(eb_id).get()
        assert eb_state["status"] == "FINISHED"


def test_two_ebs(subar_config):
    """
    Test subarray device configuration interface.
    Subarray works with execution blocks and
    their processing blocks. Create two EBs.
    """
    # Initialisation
    for txn in subar_config.txn():
        txn._txn.script.create(SCRIPT)
        txn.create_if_not_present(
            DevState.OFF, ObsState.EMPTY, AdminMode.ONLINE
        )

    # Set the processing controller and helm deployer to be online
    for txn in subar_config.txn():
        component_online(txn, "proccontrol", True)
        component_online(txn, "helmdeploy", True)

    # Create first EB and its PBs
    eb_id = "eb-xxx-20240413-00000"
    pb_id = "pb-xxx-20240413-00000"
    for txn in subar_config.txn():
        eblock = fake_eb(eb_id)
        pblock = fake_pb(pb_id)
        txn.create_eb(eblock)
        txn.create_pbs([pblock])

    # Select scan type and set scan ID
    for txn in subar_config.txn():
        txn.eb_scan_type = "xxx"
        txn.eb_scan_id = 456

    # Finish EB
    for txn in subar_config.txn():
        txn.finish_eb()

    # Create second EB and its PBs
    eb_id = "eb-yyy-20240413-00000"
    pb_id = "pb-yyy-20240413-00000"
    for txn in subar_config.txn():
        eblock = fake_eb(eb_id)
        pblock = fake_pb(pb_id)
        txn.create_eb(eblock)
        txn.create_pbs([pblock])

    for txn in subar_config.txn():
        assert txn.eb_scan_type is None
        assert txn.eb_scan_id is None
        assert txn.eb_receive_addresses == {}

        # Use low-level EB interface to check EB
        eblock = txn._txn.execution_block.get(eb_id)
        assert eblock.subarray_id == SUBARRAY_ID
        eb_state = txn._txn.execution_block.state(eb_id).get()
        assert eb_state["status"] == "ACTIVE"

    # Add a new scan type, select it and set scan ID
    for txn in subar_config.txn():
        txn.eb_scan_type = "yyy"
        txn.eb_scan_id = 789

    for txn in subar_config.txn():
        assert txn.eb_scan_type == "yyy"
        assert txn.eb_scan_id == 789

    # Cancel EB
    for txn in subar_config.txn():
        txn.cancel_eb()

    for txn in subar_config.txn():
        assert txn.eb_scan_type is None
        assert txn.eb_scan_id is None
        assert txn.eb_receive_addresses == {}
        # Use low-level EB interface to check EB
        eblock = txn._txn.execution_block.get(eb_id)
        assert eblock.subarray_id is None
        eb_state = txn._txn.execution_block.state(eb_id).get()
        assert eb_state["status"] == "CANCELLED"


@pytest.mark.parametrize(
    "command, obs_state",
    [
        ("AssignResources", ObsState.RESOURCING),
        ("ReleaseResources", ObsState.RESOURCING),
        ("ReleaseAllResources", ObsState.RESOURCING),
        ("Configure", ObsState.CONFIGURING),
        ("Abort", ObsState.ABORTING),
        ("Restart", ObsState.RESTARTING),
    ],
)
def test_transitional_obs_state(subar_config, command, obs_state):
    """
    Test transitional obs_state is correctly reported for command.
    """
    # Initialisation
    for txn in subar_config.txn():
        txn._txn.script.create(SCRIPT)
        txn.create_if_not_present(
            DevState.OFF, ObsState.EMPTY, AdminMode.ONLINE
        )

    # Set command
    for txn in subar_config.txn():
        txn.command = command

    # Test obs_state
    for txn in subar_config.txn():
        assert txn.obs_state == obs_state


def test_error_messages(subar_config):
    """
    Test that error_messages attribute correctly aggregates
    multiple error messages for the same PB as well as
    error messages from different PBs without reformatting the messages.
    """
    # Initialisation
    for txn in subar_config.txn():
        txn._txn.script.create(SCRIPT)
        txn.create_if_not_present(
            DevState.OFF, ObsState.EMPTY, AdminMode.ONLINE
        )

    # Set the processing controller and helm deployer to be online
    for txn in subar_config.txn():
        component_online(txn, "proccontrol", True)
        component_online(txn, "helmdeploy", True)

    # Create EB and its PBs
    eb_id = "eb-xxx-20240413-00000"
    pb_id1 = "pb-xxx-20240413-00001"
    pb_id2 = "pb-xxx-20240413-00002"
    for txn in subar_config.txn():
        eblock = fake_eb(eb_id)
        pblock1 = fake_pb(pb_id1)
        pblock2 = fake_pb(pb_id2)
        txn.create_eb(eblock)
        txn.create_pbs([pblock1, pblock2])

    # Define error messages for the processing blocks
    pb_id1_error_messages = {
        "proc-pb-xxx-20240413-00001-pointing-offset-first": [
            {
                "container_name": "test-pointing-first",
                "message": "Back-off pulling image",
                "reason": "ImagePullBackOff",
                "state": "waiting",
            }
        ],
        "proc-pb-xxx-20240413-00001-pointing-offset-second": [
            {
                "container_name": "test-pointing-second",
                "message": "Container crashed",
                "reason": "CrashLoopBackOff",
                "state": "waiting",
            }
        ],
    }

    pb_id2_error_messages = {
        "proc-pb-xxx-20240413-00002-vis-receive": [
            {
                "container_name": "test-vis-receive",
                "message": "Failed due to insufficient resources",
                "reason": "ResourceExhausted",
                "state": "waiting",
            }
        ]
    }

    # Create PB states with multiple error messages for the same PB
    for txn in subar_config.txn():
        txn._txn.processing_block.state(pb_id1).create(
            {"error_messages": pb_id1_error_messages}
        )

    # Add another error message for a different PB
    for txn in subar_config.txn():
        txn._txn.processing_block.state(pb_id2).create(
            {"error_messages": pb_id2_error_messages}
        )

    # Now check if the error messages have been aggregated correctly
    for txn in subar_config.txn():
        result = txn.eb_error_messages
        assert result == {
            pb_id1: pb_id1_error_messages,
            pb_id2: pb_id2_error_messages,
        }

    # Clearing the error messages for pb_id1
    for txn in subar_config.txn():
        pb1_state = txn._txn.processing_block.state(pb_id1).get()
        pb1_state["error_messages"] = {}
        txn._txn.processing_block.state(pb_id1).update(pb1_state)

    # Check that pb_id1 is now removed from the error_messages_dict
    for txn in subar_config.txn():
        result = txn.eb_error_messages
        assert result == {pb_id2: pb_id2_error_messages}


def test_receive_addresses(subar_config):
    """
    Test that receive_addresses are correctly merged
    from different processing block states.
    """
    # Initialisation
    for txn in subar_config.txn():
        txn._txn.script.create(SCRIPT)
        txn.create_if_not_present(
            DevState.OFF, ObsState.EMPTY, AdminMode.ONLINE
        )

    # Set the processing controller and helm deployer to be online
    for txn in subar_config.txn():
        component_online(txn, "proccontrol", True)
        component_online(txn, "helmdeploy", True)

    # Create EB and its PBs
    eb_id = "eb-xxx-20240413-00000"
    pb_id1 = "pb-xxx-20240413-00001"
    pb_id2 = "pb-xxx-20240413-00002"
    for txn in subar_config.txn():
        eblock = fake_eb(eb_id)
        pblock1 = fake_pb(pb_id1)
        pblock2 = fake_pb(pb_id2)
        txn.create_eb(eblock)
        txn.create_pbs([pblock1, pblock2])

    # create PB states with receive_addresses
    for txn in subar_config.txn():
        txn._txn.processing_block.state(pb_id1).create(
            {
                "receive_addresses": {
                    "pointing": {"vis0": {"host": "my-host", "port": []}},
                    "science": {
                        "pss1": {
                            "host": "your-host",
                            "port": [0],
                            "function": "pss",
                        }
                    },
                }
            },
        )
        txn._txn.processing_block.state(pb_id2).create(
            {
                "receive_addresses": {
                    "pointing": {"vis0": {"pointing_cal": "test"}},
                    "science": {
                        "pss1": {"pointing_cal": "test", "function": "pss"}
                    },
                }
            },
        )

    for txn in subar_config.txn():
        result = txn.eb_receive_addresses
        assert result == {
            "pointing": {
                "vis0": {"host": "my-host", "port": [], "pointing_cal": "test"}
            },
            "science": {
                "pss1": {
                    "host": "your-host",
                    "port": [0],
                    "pointing_cal": "test",
                    "function": "pss",
                }
            },
        }


def test_invalid_attributes(subar_config):
    """
    Test subarray device configuration interface.
    When state_commanded, admin_mode and obs_state_commanded and state are set
    to an invalid value, they return None when queried.
    """
    # Initialisation
    for txn in subar_config.txn():
        txn._txn.script.create(SCRIPT)
        txn.create_if_not_present(
            DevState.OFF, ObsState.EMPTY, AdminMode.ONLINE
        )

    # Test invalid state and obsState values
    for txn in subar_config.txn():
        # Use low-level subarray interface to set invalid state and obs_state
        subarray = txn._txn.self.get()
        subarray["state_commanded"] = "invalid"
        subarray["admin_mode"] = "invalid"
        subarray["obs_state_commanded"] = "invalid"
        txn._txn.self.update(subarray)

    for txn in subar_config.txn():
        assert txn.state_commanded is None
        assert txn.state is None
        assert txn.admin_mode is None
        assert txn.obs_state_commanded is None
        assert txn.obs_state is None


def test_missing_state_keys_are_handled(subar_config):
    """
    SubarrayState._get() handles missing keys
    with allowing for a default return value.

    examples:
    "resources", if not present should return {}
    "eb_id", if not present should return the default None
    """
    missing_keys = ["resources", "eb_id"]
    # Initialisation
    for txn in subar_config.txn():
        txn.create_if_not_present(
            DevState.OFF, ObsState.EMPTY, AdminMode.ONLINE
        )

    # Set up: remove some keys from the state
    for txn in subar_config.txn():
        subarray = txn._txn.self.get()
        for k in missing_keys:
            del subarray[k]
        txn._txn.self.update(subarray)

    for txn in subar_config.txn():
        subarray = txn._txn.self.get()
        for k in missing_keys:
            assert k not in subarray.keys()

    # Check that even though they are not present,
    # the expected default is returned
    for txn in subar_config.txn():
        assert txn.resources == {}
        assert txn.eb_id is None


def test_missing_eb_is_handled(subar_config):
    """
    Test SubarrayState _get_eb and _set_eb methods do the right thing if the EB
    ID is set but the corresponding EB entry does not exist.
    """
    # Create subarray entry
    for txn in subar_config.txn():
        txn.create_if_not_present(
            DevState.OFF, ObsState.EMPTY, AdminMode.ONLINE
        )
        # Set EB ID to that of non-existent EB
        txn._set("eb_id", "eb-nonexistent-00000000-00000")

    # Test getting value
    for txn in subar_config.txn():
        assert txn._get_eb("foo", "bar") == "bar"

    # Test setting value
    for txn in subar_config.txn():
        with pytest.raises(DevFailed):
            txn._set_eb("foo", "bar")


def test_eb_scans(subar_config):
    """SubarrayState stores completed scans."""

    # Initialisation
    for txn in subar_config.txn():
        txn._txn.script.create(SCRIPT)
        txn.create_if_not_present(
            DevState.OFF, ObsState.EMPTY, AdminMode.ONLINE
        )

    # Set the processing controller and helm deployer to be online
    for txn in subar_config.txn():
        component_online(txn, "proccontrol", True)
        component_online(txn, "helmdeploy", True)

    # Create EB (with scan types "xxx" and "yyy") and single PB
    eb_id = "eb-xxx-20240413-00000"
    pb_id = "pb-xxx-20240413-00000"
    for txn in subar_config.txn():
        eblock = fake_eb(eb_id)
        pblock = fake_pb(pb_id)
        txn.create_eb(eblock)
        txn.create_pbs([pblock])

    # Set scan type and scan ID and add scan to list
    for txn in subar_config.txn():
        txn.eb_scan_type = "xxx"
        txn.eb_scan_id = 1
        txn.eb_add_current_scan("FINISHED")
        txn.eb_scan_id = 2
        txn.eb_add_current_scan("ABORTED")
        txn.eb_scan_type = "yyy"
        txn.eb_scan_id = 3
        txn.eb_add_current_scan("FINISHED")
        txn.eb_scan_id = 4
        txn.eb_add_current_scan("ABORTED")
        # Raise exception if scan ID is not set
        txn.eb_scan_id = None
        with pytest.raises(DevFailed):
            txn.eb_add_current_scan("FINISHED")
        # Raise exception if scan type is not set
        txn.eb_scan_type = None
        with pytest.raises(DevFailed):
            txn.eb_add_current_scan("FINISHED")

    # Check value of scans
    expected_scans = [
        {"scan_id": 1, "scan_type": "xxx", "status": "FINISHED"},
        {"scan_id": 2, "scan_type": "xxx", "status": "ABORTED"},
        {"scan_id": 3, "scan_type": "yyy", "status": "FINISHED"},
        {"scan_id": 4, "scan_type": "yyy", "status": "ABORTED"},
    ]
    for txn in subar_config.txn():
        for idx, scan in enumerate(txn.eb_scans):
            assert scan == expected_scans[idx]


@pytest.mark.parametrize(
    "proccontrol_online, helmdeploy_online, health_state",
    [
        (True, True, HealthState.OK),
        (False, True, HealthState.DEGRADED),
        (True, False, HealthState.DEGRADED),
        (False, False, HealthState.DEGRADED),
    ],
)
def test_components_health_state(
    subar_config,
    proccontrol_online,
    helmdeploy_online,
    health_state,
):
    """
    Test health_state is set based on the status of the components.
    """
    # Initialisation
    for txn in subar_config.txn():
        txn.create_if_not_present(
            DevState.OFF, ObsState.EMPTY, AdminMode.ONLINE
        )

    # Add components and their alive key
    for txn in subar_config.txn():
        component_online(txn, "proccontrol", proccontrol_online)
        component_online(txn, "helmdeploy", helmdeploy_online)

    for txn in subar_config.txn():
        assert txn.health_state == health_state


@pytest.mark.parametrize(
    "script_version",
    ["==1.2.3", "<=1.2.3", ">=1.2.3", ">1.0.0, <=2.0.0", ">=0.9.0", None],
)
def test_script_version_valid(subar_config, script_version):
    """
    Test script version vs SDP version (=1.2.3 from fixture)
    Valid conditions
    """
    eb_id = "eb-xxx-20240821-00000"
    pb_id = "pb-xxx-20240821-00000"
    for txn in subar_config.txn():
        txn._txn.script.create(SCRIPT)
        txn.create_if_not_present(
            DevState.OFF, ObsState.EMPTY, AdminMode.ONLINE
        )
        component_online(txn, "proccontrol", True)
        component_online(txn, "helmdeploy", True)
        eblock = fake_eb(eb_id)
        pblock = fake_pb(pb_id)
        txn.create_eb(eblock)

        SCRIPT.sdp_version = script_version
        txn._txn.script.update(SCRIPT)
        txn.create_pbs([pblock])


@pytest.mark.parametrize(
    "script_version",
    [
        "<1.2.3",
        "<=1.2.2",
        ">=1.2.4",
        "==2.0.0",
        ">=3.0.1",
        "<=1.2.2, >=1.2.4",
        "<1.2.1, >2.0.4",
    ],
)
def test_script_version_invalid(subar_config, script_version):
    """
    Test script version vs SDP version (=1.2.3 from fixture)
    Test script version invalid.
    """
    eb_id = "eb-xxx-20240821-00000"
    pb_id = "pb-xxx-20240821-00000"
    for txn in subar_config.txn():
        txn._txn.script.create(SCRIPT)
        txn.create_if_not_present(
            DevState.OFF, ObsState.EMPTY, AdminMode.ONLINE
        )
        component_online(txn, "proccontrol", True)
        component_online(txn, "helmdeploy", True)
        eblock = fake_eb(eb_id)
        pblock = fake_pb(pb_id)
        txn.create_eb(eblock)

        SCRIPT.sdp_version = script_version
        txn._txn.script.update(SCRIPT)

        with pytest.raises(DevFailed):
            txn.create_pbs([pblock])


# Helper functions


def fake_eb(eb_id):
    """Generate dict representing fake execution block."""
    eblock = {
        "eb_id": eb_id,
        "scan_types": [
            {"scan_type_id": "xxx"},
            {"scan_type_id": "yyy"},
        ],
    }
    return eblock


def fake_pb(pb_id):
    """Generate dict representing fake processing block."""
    pblock = {
        "pb_id": pb_id,
        "script": SCRIPT_KEY,
        "parameters": {},
        "dependencies": [],
    }
    return pblock
