"""Pytest fixtures."""

import pytest
from ska_sdp_config.entity import System
from tango.test_context import MultiDeviceTestContext

from ska_sdp_lmc import SDPController, SDPSubarray
from ska_sdp_lmc.config import FEATURE_CONFIG_DB
from ska_sdp_lmc.device import FEATURE_EVENT_LOOP, devices_initialised
from ska_sdp_lmc.subarray.config import subarray_config
from ska_sdp_lmc.subarray.validation import FEATURE_STRICT_VALIDATION
from ska_sdp_lmc.tango_logging import configure_root_logger

configure_root_logger()

# Use the config DB memory backend in the devices. This will be overridden if
# the FEATURE_CONFIG_DB environment variable is set to 1.
FEATURE_CONFIG_DB.set_default(False)
# Disable the event loop in the devices. This will be overridden if the
# FEATURE_EVENT_LOOP environment variable is set to 1.
FEATURE_EVENT_LOOP.set_default(False)
# Enable strict validation of subarray schemas. This will be overridden if the
# FEATURE_STRICT_VALIDATION environment variable is set to 0.
FEATURE_STRICT_VALIDATION.set_default(True)


# List of devices for the test session
device_info = [
    {"class": SDPController, "devices": [{"name": "test-sdp/control/0"}]},
    {"class": SDPSubarray, "devices": [{"name": "test-sdp/subarray/01"}]},
]


@pytest.fixture(scope="session")
def devices() -> MultiDeviceTestContext:
    """Start the devices in a MultiDeviceTestContext."""
    context = MultiDeviceTestContext(device_info)
    context.start()
    devices_initialised(SDPController)
    devices_initialised(SDPSubarray)
    yield context
    context.stop()


SYSTEM = System(version="1.2.3", components={}, dependencies={})
SUBARRAY_NAME = "test-sdp/subarray/01"


@pytest.fixture(scope="function", name="subar_config")
def subar_config_fixt():
    """Subarray configuration"""
    with subarray_config(SUBARRAY_NAME) as cfg:
        cfg.backend.delete("/component", must_exist=False, recursive=True)
        cfg.backend.delete("/lmc", must_exist=False, recursive=True)
        cfg.backend.delete("/eb", must_exist=False, recursive=True)
        cfg.backend.delete("/pb", must_exist=False, recursive=True)
        cfg.backend.delete("/script", must_exist=False, recursive=True)
        cfg.backend.delete("/system", must_exist=False, recursive=True)
        for txn in cfg.txn():
            # pylint: disable-next=protected-access
            txn._txn.system.create(SYSTEM)

        yield cfg

        cfg.backend.delete("/component", must_exist=False, recursive=True)
        cfg.backend.delete("/lmc", must_exist=False, recursive=True)
        cfg.backend.delete("/eb", must_exist=False, recursive=True)
        cfg.backend.delete("/pb", must_exist=False, recursive=True)
        cfg.backend.delete("/script", must_exist=False, recursive=True)
        cfg.backend.delete("/system", must_exist=False, recursive=True)
