"""Test controller configuration interface."""

# pylint: disable=protected-access
# pylint: disable=too-many-arguments
# pylint: disable=too-many-positional-arguments

import pytest
from ska_control_model import AdminMode, HealthState
from ska_sdp_config.entity.system import System, SystemComponent
from tango import DevState

from ska_sdp_lmc.controller.config import (
    FAULTY_COMP_MESSAGE,
    controller_config,
)

SDP_VERSION = System(
    version="1.2.3",
    components={
        "proccontrol": SystemComponent(
            image="artefact.skao.int/ska-sdp-proccontrol", version="4.5.6"
        ),
        "helmdeploy": SystemComponent(
            image="artefact.skao.int/ska-sdp-helmdeploy", version="7.8.9"
        ),
    },
    dependencies={},
)


@pytest.fixture(scope="function", name="contr_config")
def contr_config_fixt():
    """Controller configuration"""
    with controller_config() as cfg:
        cfg.backend.delete("/system", must_exist=False)
        cfg.backend.delete("/component", must_exist=False, recursive=True)
        yield cfg
        cfg.backend.delete("/system", must_exist=False)
        cfg.backend.delete("/component", must_exist=False, recursive=True)


def test_initialisation(contr_config):
    """
    Test controller device configuration interface.
    Device is initialised correctly.
    """
    # Initialisation
    for txn in contr_config.txn():
        txn.create_if_not_present(DevState.STANDBY, AdminMode.ONLINE)

    for txn in contr_config.txn():
        assert txn.transaction_id is None
        assert txn.state_commanded == DevState.STANDBY
        assert txn.state == DevState.STANDBY
        assert txn.admin_mode == AdminMode.ONLINE


def test_update_attributes(contr_config):
    """
    Test controller device configuration interface.
    Attributes are updated correctly.
    """
    # Initialisation
    for txn in contr_config.txn():
        txn.create_if_not_present(DevState.STANDBY, AdminMode.ONLINE)

    # Set transaction ID, device state and admin mode
    for txn in contr_config.txn():
        txn.transaction_id = "txn-aaa"
        txn.state_commanded = DevState.ON
        txn.admin_mode = AdminMode.ENGINEERING

    for txn in contr_config.txn():
        assert txn.transaction_id == "txn-aaa"
        assert txn.state_commanded == DevState.ON
        assert txn.state == DevState.ON
        assert txn.admin_mode == AdminMode.ENGINEERING


def test_invalid_attributes(contr_config):
    """
    Test controller device configuration interface.
    When state_commanded and admin_mode are set to an invalid value, they
    return None when queried.
    """
    # Initialisation
    for txn in contr_config.txn():
        txn.create_if_not_present(DevState.STANDBY, AdminMode.ONLINE)

    # Set invalid device state and admin mode
    for txn in contr_config.txn():
        # Use low-level controller interface
        controller = txn._txn.self.get()
        controller["state_commanded"] = "invalid"
        controller["admin_mode"] = "invalid"
        txn._txn.self.update(controller)

    for txn in contr_config.txn():
        assert txn.state_commanded is None
        assert txn.state is None
        assert txn.admin_mode is None


@pytest.mark.parametrize(
    "admin_mode, disabled",
    [
        (AdminMode.ONLINE, False),
        (AdminMode.OFFLINE, True),
        (AdminMode.ENGINEERING, False),
        (AdminMode.NOT_FITTED, True),
        (AdminMode.RESERVED, True),
    ],
)
@pytest.mark.parametrize(
    "state", [DevState.OFF, DevState.STANDBY, DevState.ON]
)
def test_admin_mode(contr_config, state, admin_mode, disabled):
    """
    When admin_mode is set to OFFLINE, NOT_FITTED or RESERVED then the state is
    reported as DISABLE, otherwise it is the commanded state.
    """
    # Initialisation
    for txn in contr_config.txn():
        txn.create_if_not_present(state, admin_mode)

    state_expected = DevState.DISABLE if disabled else state
    for txn in contr_config.txn():
        assert txn.state == state_expected


@pytest.mark.parametrize(
    "subarray1_online, subarray2_online, proccontrol_online, "
    "helmdeploy_online, health_state",
    [
        (True, True, True, True, HealthState.OK),
        (False, True, True, True, HealthState.DEGRADED),
        (True, False, True, True, HealthState.DEGRADED),
        (False, False, True, True, HealthState.FAILED),
        (True, True, False, True, HealthState.DEGRADED),
        (True, True, True, False, HealthState.DEGRADED),
        (True, True, False, False, HealthState.DEGRADED),
        (False, False, False, False, HealthState.FAILED),
    ],
)
# pylint: disable=too-many-positional-arguments
def test_components_health_state(
    contr_config,
    subarray1_online,
    subarray2_online,
    proccontrol_online,
    helmdeploy_online,
    health_state,
):
    """
    Test components and health_state are set based on the status of the
    components.
    """
    # Initialisation
    for txn in contr_config.txn():
        txn.create_if_not_present(DevState.STANDBY, AdminMode.ONLINE)

    # Add components and their alive key
    for txn in contr_config.txn():
        txn._txn.component("lmc-subarray-01").create({})
        txn._txn.component("lmc-subarray-02").create({})
        component_online(txn, "lmc-controller", True)
        component_online(txn, "lmc-subarray-01", subarray1_online)
        component_online(txn, "lmc-subarray-02", subarray2_online)
        component_online(txn, "proccontrol", proccontrol_online)
        component_online(txn, "helmdeploy", helmdeploy_online)

    for txn in contr_config.txn():
        assert txn.components == {
            "lmc-controller": component_status("lmc-controller", True),
            "lmc-subarray-01": component_status(
                "lmc-subarray", subarray1_online
            ),
            "lmc-subarray-02": component_status(
                "lmc-subarray", subarray2_online
            ),
            "proccontrol": component_status("proccontrol", proccontrol_online),
            "helmdeploy": component_status("helmdeploy", helmdeploy_online),
        }
        assert txn.health_state == health_state


def test_sdp_version(contr_config):
    """SDP version information."""
    # Initialisation
    for txn in contr_config.txn():
        txn.create_if_not_present(DevState.STANDBY, AdminMode.ONLINE)

    # Test that error is returned
    for txn in contr_config.txn():
        assert "error" in txn.sdp_version

    # Add version information
    for txn in contr_config.txn():
        txn._txn.system.create(SDP_VERSION)

    # Test that version is returned correctly
    for txn in contr_config.txn():
        assert txn.sdp_version == SDP_VERSION.model_dump()


# Helper functions


def component_online(txn, component, online):
    """Create owner key for component if online."""
    if online:
        txn._txn.component(component).ownership.take()


def component_status(component, online):
    """Status message for component."""
    if online:
        status = {"status": "ONLINE"}
    else:
        status = {
            "status": "OFFLINE",
            "error": FAULTY_COMP_MESSAGE[component],
        }
    return status
