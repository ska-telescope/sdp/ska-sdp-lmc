"""Test controller device."""

# pylint: disable=protected-access
# pylint: disable=too-many-arguments
# pylint: disable=too-many-positional-arguments

import json

import pytest
from device_utils import (
    init_device,
    log_contains_device_name,
    log_contains_transaction_id,
    update_attributes,
    wait_for,
    wait_for_admin_mode,
    wait_for_health_state,
    wait_for_state,
)
from ska_control_model import AdminMode, HealthState
from ska_sdp_config import ConfigCollision, ConfigVanished
from ska_sdp_config.entity.system import System, SystemComponent
from tango import DevFailed, DevState, DevVoid

from ska_sdp_lmc import config
from ska_sdp_lmc.controller import config as controller_config

DEVICE_NAME = "test-sdp/control/0"
COMPONENT_NAME = "lmc-controller"

SDP_VERSION = System(
    version="1.2.3",
    components={
        "helmdeploy": SystemComponent(
            image="artefact.skao.int/ska-sdp-helmdeploy", version="7.8.9"
        ),
        "lmc-controller": SystemComponent(
            image="artefact.skao.int/ska-sdp-lmc", version="1.2.3"
        ),
        "lmc-subarray-01": SystemComponent(
            image="artefact.skao.int/ska-sdp-lmc", version="1.2.3"
        ),
        "lmc-subarray-02": SystemComponent(
            image="artefact.skao.int/ska-sdp-lmc", version="1.2.3"
        ),
        "lmc-subarray-03": SystemComponent(
            image="artefact.skao.int/ska-sdp-lmc", version="1.2.3"
        ),
        "proccontrol": SystemComponent(
            image="artefact.skao.int/ska-sdp-proccontrol", version="4.5.6"
        ),
    },
    dependencies={},  # Empty dictionary for dependencies
)


COMMANDS = ["Off", "Standby", "On"]


@pytest.fixture(name="config_db_client", scope="module")
def get_config_db_client():
    """Config DB client instance used throughout these tests."""
    with config.new_config_db_client(COMPONENT_NAME) as cfg:
        yield cfg


@pytest.fixture(name="controller_device")
def get_controller_device(devices):
    """Get controller device."""
    return init_device(devices, DEVICE_NAME)


@pytest.mark.parametrize(
    "command, input_type, output_type",
    [(command, DevVoid, DevVoid) for command in COMMANDS],
)
def test_commands(controller_device, command, input_type, output_type):
    """Command is present and has correct input and output types."""
    assert command in controller_device.get_command_list()
    command_config = controller_device.get_command_config(command)
    assert command_config.in_type == input_type
    assert command_config.out_type == output_type


def test_initialisation(caplog, config_db_client, controller_device):
    """Device is initialised in the correct state."""
    # Remove the controller state
    config_db_client.backend.delete(
        f"/component/{COMPONENT_NAME}", recursive=True, must_exist=False
    )
    # Call the Init command to reinitialise the device
    controller_device.Init()
    # Update the attributes if the event loop is not running
    update_attributes(controller_device)
    # State become STANDBY
    assert wait_for_state(controller_device, DevState.STANDBY)
    # Admin mode is ONLINE
    assert controller_device.adminMode == AdminMode.ONLINE
    # Component is alive
    assert config_db_client.is_alive()
    # Log contains device name
    assert log_contains_device_name(caplog, DEVICE_NAME)
    # Log does not contain transaction ID
    assert not log_contains_transaction_id(caplog)


def test_recover_lost_connection(config_db_client, controller_device):
    """Device recovers from lost connection."""
    config_db_client.backend.delete("/component/" + COMPONENT_NAME + "/owner")
    update_attributes(controller_device)
    assert wait_for(config_db_client.is_alive)


def test_sdp_version_error(config_db_client, controller_device):
    """SDP version shows an error if the information is not present."""
    config_db_client.backend.delete("/system", must_exist=False)
    update_attributes(controller_device)
    assert wait_for(
        lambda: "error" in json.loads(controller_device.sdpVersion)
    )


def test_sdp_version_correct(config_db_client, controller_device):
    """SDP version is correct when the information is present."""
    try:
        for txn in config_db_client.txn():
            txn.system.create(SDP_VERSION)
    except ConfigCollision:
        pass
    update_attributes(controller_device)
    assert wait_for(
        lambda: json.loads(controller_device.sdpVersion)
        == SDP_VERSION.model_dump()
    )


def test_sdp_version_with_deps(config_db_client, controller_device):
    """
    In ska-sdp-config 0.10.0 System.dependencies[...]["repository"]
    is defined as a pydantic_core.Url object. This breaks
    json.dumps in controller_device._set_sdp_version.
    Here we test the fix.
    """
    sdp_version = SDP_VERSION.copy()
    sdp_version.dependencies = {
        "ska-tango-tangogql": {
            "repository": "https://artefact.skao.int/repository/helm-internal",
            "version": "1.4.5",
        }
    }
    for txn in config_db_client.txn():
        txn.raw.delete("/system", must_exist=False)
    for txn in config_db_client.txn():
        txn.system.create(sdp_version)

    # need to load to string then back to dict because
    # model_dump loads url-type dependencies as Url object,
    # while controller_device.sdpVersion loads them as strings,
    # which are not equal.
    expected_result = json.loads(sdp_version.model_dump_json())

    update_attributes(controller_device)
    assert wait_for(
        lambda: json.loads(controller_device.sdpVersion) == expected_result
    )


@pytest.mark.parametrize(
    "admin_mode, disabled",
    [
        (AdminMode.ONLINE, False),
        (AdminMode.OFFLINE, True),
        (AdminMode.ENGINEERING, False),
        (AdminMode.NOT_FITTED, True),
        (AdminMode.RESERVED, True),
    ],
)
@pytest.mark.parametrize(
    "commanded_state", [DevState.OFF, DevState.STANDBY, DevState.ON]
)
def test_set_admin_mode(
    config_db_client, controller_device, commanded_state, admin_mode, disabled
):
    """Admin mode can be set and modifies state."""
    set_state(config_db_client, controller_device, commanded_state)
    set_admin_mode(controller_device, admin_mode)
    expected_state = DevState.DISABLE if disabled else commanded_state
    assert controller_device.State() == expected_state


@pytest.mark.parametrize(
    "commanded_state, command",
    [
        (DevState.STANDBY, "Off"),
        (DevState.ON, "Off"),
        (DevState.OFF, "Standby"),
        (DevState.ON, "Standby"),
        (DevState.OFF, "On"),
        (DevState.STANDBY, "On"),
    ],
)
@pytest.mark.parametrize(
    "admin_mode",
    [AdminMode.OFFLINE, AdminMode.NOT_FITTED, AdminMode.RESERVED],
)
def test_command_rejected_admin_mode(
    config_db_client, controller_device, admin_mode, commanded_state, command
):
    """Command is rejected when admin mode sets state to DISABLE."""
    set_state(config_db_client, controller_device, commanded_state)
    set_admin_mode(controller_device, admin_mode)
    command_raises_exception(
        controller_device, command, "API_CommandNotAllowed"
    )
    set_admin_mode(controller_device, AdminMode.ONLINE)


@pytest.mark.parametrize(
    "command, initial_state, final_state",
    [
        ("Off", DevState.STANDBY, DevState.OFF),
        ("Off", DevState.ON, DevState.OFF),
        ("Standby", DevState.OFF, DevState.STANDBY),
        ("Standby", DevState.ON, DevState.STANDBY),
        ("On", DevState.OFF, DevState.ON),
        ("On", DevState.STANDBY, DevState.ON),
    ],
)
def test_command_succeeds(
    caplog,
    config_db_client,
    controller_device,
    command,
    initial_state,
    final_state,
):
    """Command succeeds in allowed state."""
    set_state(config_db_client, controller_device, initial_state)
    call_command(controller_device, command)
    assert controller_device.State() == final_state
    assert log_contains_transaction_id(caplog)


@pytest.mark.parametrize(
    "command, initial_state",
    [
        ("Off", DevState.OFF),
        ("Standby", DevState.STANDBY),
        ("On", DevState.ON),
    ],
)
def test_command_rejected_state(
    config_db_client, controller_device, command, initial_state
):
    """Command is rejected in disallowed state."""
    set_state(config_db_client, controller_device, initial_state)
    command_raises_exception(
        controller_device, command, "API_CommandNotAllowed"
    )


@pytest.mark.parametrize(
    "subarray2_online, subarray3_online, proccontrol_online, "
    "helmdeploy_online, health_state",
    [
        (True, True, True, True, HealthState.OK),
        (False, True, True, True, HealthState.DEGRADED),
        (True, False, True, True, HealthState.DEGRADED),
        (False, False, True, True, HealthState.DEGRADED),
        (True, True, False, True, HealthState.DEGRADED),
        (True, True, True, False, HealthState.DEGRADED),
        (True, True, False, False, HealthState.DEGRADED),
        (False, False, False, False, HealthState.DEGRADED),
    ],
)
# pylint: disable=too-many-positional-arguments
def test_components_health_state(
    config_db_client,
    controller_device,
    subarray2_online,
    subarray3_online,
    proccontrol_online,
    helmdeploy_online,
    health_state,
):
    """Health state is correctly updated based on component status."""
    set_component_status(
        config_db_client,
        subarray2_online,
        subarray3_online,
        proccontrol_online,
        helmdeploy_online,
    )
    update_attributes(controller_device)
    assert wait_for_health_state(controller_device, health_state)


# Helper functions


def set_state(config_db_client, controller_device, state_commanded):
    """Set the device state."""
    for txn in config_db_client.txn():
        controller = controller_config.ControllerState(txn)
        controller.transaction_id = None
        controller.state_commanded = state_commanded
        controller.admin_mode = AdminMode.ONLINE
    update_attributes(controller_device)
    assert wait_for_state(controller_device, state_commanded)


def set_admin_mode(controller_device, admin_mode):
    """Set adminMode of the device."""
    controller_device.adminMode = admin_mode
    update_attributes(controller_device)
    assert wait_for_admin_mode(controller_device, admin_mode)


def call_command(controller_device, command):
    """Call command."""
    assert command in controller_device.get_command_list()
    controller_device.command_inout(command)


def command_raises_exception(controller_device, command, reason):
    """Call command and check that it raises exception."""
    assert command in controller_device.get_command_list()
    with pytest.raises(DevFailed) as exception:
        controller_device.command_inout(command)
    assert exception.value.args[0].reason == reason


def set_component_status(
    config_db_client,
    subarray2_online,
    subarray3_online,
    proccontrol_online,
    helmdeploy_online,
):
    """
    Create extra subarray entries and create/delete owner entries of
    components.
    """
    for txn in config_db_client.txn():
        create_subarray(txn, "02")
        create_subarray(txn, "03")
        set_owner(txn, "lmc-subarray-02", subarray2_online)
        set_owner(txn, "lmc-subarray-03", subarray3_online)
        set_owner(txn, "proccontrol", proccontrol_online)
        set_owner(txn, "helmdeploy", helmdeploy_online)


def create_subarray(txn, subarray_id):
    """Create subarray entry if it does not exist."""
    try:
        txn.component(f"lmc-subarray-{subarray_id}").create({})
    except ConfigCollision:
        pass


def set_owner(txn, component, online):
    """
    Create owner key if required status is ONLINE.
    Remove owner key if required status is OFFLINE.
    """
    if online:
        try:
            txn.component(component).ownership.take()
        except ConfigCollision:
            pass
    else:
        try:
            txn.raw.delete(f"/component/{component}/owner")
        except ConfigVanished:
            pass
