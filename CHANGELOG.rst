Changelog
=========

1.0.0
-----

- Updated dependencies
  (`MR141 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/141>`__)
- Use version 1.0 of SDP command schemas and support version 0.4 and 0.5. Remove support for version 0.3.
  (`MR142 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/142>`__)
- Updated dependencies, including ska-sdp-config 1.0.0
  (`MR138 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/138>`__)
  (`MR141 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/141>`__)
- Add schema validation tests
  (`MR143 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/143>`__)
- Allow release candidate versions of SDP for script validation
  (`MR139 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/139>`__)
- Use SKA theme in documentation
  (`MR137 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/137>`__)
- Update Dockerfile to use SKA Python base image
  (`MR135 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/135>`__)

0.32.1
------

- Use ska-sdp-config 0.10.2
  (`MR136 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/136>`__)

  - Fixes SKB-685: txn.update is only committed if there is a difference
    between existing and new value

- Set log level using SDP_LOG_LEVEL environment variable
  (`MR134 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/134>`__)
- Refactor logging
  (`MR134 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/134>`__)
- Refactor device initialisation and remove unused code
  (`MR133 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/133>`__)

.. _section-1:

0.32.0
------

- Updated dependencies
  (`MR132 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/132>`__):

  - ska-sdp-config 0.10.1
  - ska-control-model 1.1.1
  - ska-telmodel 1.19.7

- Update healthState and obsState depending on pb status and error
  messages
  (`MR131 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/131>`__,
  `MR132 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/132>`__)
- Reject AssignResources command with exception if processing block
  parameters are invalid
  (`MR128 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/128>`__
- Update ska-sdp-config to 0.10.0 and fix loading data from /system into
  controller.sdpVersion
  (`MR129 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/129>`__)
- errorMessages tango attribute added to the subarray device
  (`MR125 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/125>`__)

.. _section-2:

0.31.1
------

- Reject AssignResources command with exception if SDP release is
  incompatible with script version limits
  (`MR121 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/121>`__,
  `MR122 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/122>`__).

.. _section-3:

0.31.0
------

- Update ska-sdp-config to 0.9.0
  (`MR120 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/120>`__)
- Updated to use pydantic model for execution block
  (`MR116 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/116>`__\ (`MR117 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/117>`__)).
- Switched to use ska-sdp-python base image
  (`MR119 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/119>`__)

.. _section-4:

0.30.0
------

- Reject AssignResources command if scan_type_id doesn’t comply when
  derive_from key is present
  (`MR111 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/111>`__).
- Update ska-sdp-config to 0.6.0 and port code to use new API
  (`MR105 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/105>`__).

.. _section-5:

0.29.0
------

- Remove subarray ObsReset command
  (`MR104 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/104>`__).
- Add subarray healthState logic and update controller healthState logic
  (`MR106 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/106>`__).
- Reject subarray AssignResources command when processing controller or
  Helm deployer are offline which means that processing blocks cannot be
  started
  (`MR108 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/108>`__).
- Ensure subarray pushes events for transitional obsStates
  (`MR109 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/109>`__).

.. _section-6:

0.28.0
------

- Update ska-control-model to 0.3.2 and replace the deprecated
  MAINTENANCE admin mode with ENGINEERING
  (`MR97 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/97>`__).

.. _section-7:

0.27.0
------

- Update ska-sdp-config to 0.5.6 and close connection to Configuration
  DB upon exit
  (`MR94 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/94>`__).
- Update PyTango to 9.5
  (`MR79 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/79>`__).
- Push change and archive events for the ``versionId`` Tango attribute
  (`MR93 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/93>`__).
- Implement full admin mode model
  (`MR87 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/87>`__).
- Remove the Disable command from the controller device
  (`MR87 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/87>`__).

.. _section-8:

0.26.0
------

- Run the event loop test CI job on SKA GitLab runners
  (`MR81 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/81>`__).
- Use configuration library to wrap transactions
  (`MR83 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/83>`__).
- Read SDP version information from configuration database
  (`MR84 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/84>`__).
- Read subarray AssignResources and Configure timeouts from environment
  variables
  (`MR86 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/86>`__).

.. _section-9:

0.25.0
------

- Upgrade PyTango to 9.4.2
  (`MR62 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/62>`__).

.. _section-10:

0.24.1
------

- Allow dependencies between real-time processing blocks to exist
  without affecting their scheduling
  (`MR73 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/73>`__).
- Updated to have access to the FQDN in the receive_addresses subarray
  tango attribute
  (`MR75 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/75>`__)

.. _section-11:

0.24.0
------

- Add new ``sdpVersion`` attribute to controller to publish SDP version
  information and rename ``version`` attribute to ``versionId`` to
  conform to standard interface
  (`MR71 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/71>`__)

.. _section-12:

0.23.1
------

- Updated to use the new version of config DB 0.5.3 which includes a
  bugfix in delete functionality
  (`MR78 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/78>`__)

.. _section-13:

0.23.0
------

- Updated to use the new version of config DB 0.5.2
  (`MR69 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/69>`__)

.. _section-14:

0.22.0
------

- Configure command waits for all deployments to be “RUNNING” before
  transitions from “CONFIGURING” to “READY” ObsState
  (`MR63 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/63>`__,
  `MR66 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/66>`__)
- Fixed pytest-bdd 6.1.1 incompatibility
  (`MR64 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/64>`__)

.. _section-15:

0.21.1
------

- Reject AssignResources if processing script is not defined; fixes
  `SKB-211 <https://jira.skatelescope.org/browse/SKB-211>`__
  (`MR61 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/61>`__)
- Fix threading problem; fixes
  `SKB-205 <https://jira.skatelescope.org/browse/SKB-205>`__
  (`MR60 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/60>`__)
- Use ska-sdp-config 0.4.5
  (`MR60 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/60>`__)

.. _section-16:

0.21.0
------

- Set up component monitoring and healthState update with controller
  (`MR52 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/52>`__,
  `MR57 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/57>`__)
- Implement using etcd lease entries
  (`MR51 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/51>`__,
  `MR58 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/58>`__)
- Update to use pytango 9.3.6
  (`MR55 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/55>`__)
- Add updating of flag file in event loop to integrate with Kubernetes
  liveness probing
  (`MR53 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/53>`__)
- Subarray device handles non-existent execution blocks
  (`MR50 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/50>`__)

.. _section-17:

0.20.4
------

- Subarray allows Abort command in RESOURCING obsState
  (`MR49 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/49>`__)

.. _section-18:

0.20.3
------

- Subarray device will throw an error if the EB or PB IDs don’t confirm
  to validation
  (`MR46 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/46>`__)
- Tango attribute archiving related documentation was added
  (`MR45 <https://gitlab.com/ska-telescope/sdp/ska-sdp-lmc/-/merge_requests/45>`__)

.. _section-19:

0.18.1
------

- Bug fix: prevent devices crashing when they are initialised by adding
  short delay before event loop starts.

.. _section-20:

0.18.0
------

- Modify behaviour of commands so attributes take their transitional or
  final value before a command returns.

.. _section-21:

0.17.2
------

- Publish Docker image in central artefact repository.

.. _section-22:

0.17.1
------

- Bug fix: remove temporary debugging code.

.. _section-23:

0.17.0
------

- Add support for version 0.3 of the SDP interface schemas while
  retaining support for version 0.2. Commands without an interface value
  are assumed to be using version 0.2 for backwards compatibility.
