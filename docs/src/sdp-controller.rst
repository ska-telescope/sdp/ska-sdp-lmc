SDP Controller Device
=====================

Introduction
------------

The SDP controller Tango device is designed to provide the overall control and
monitoring of the SDP. Its attributes report on the overall state of the
system.

The present implementation of the SDP controller device does the following:

- performs the state transitions in response to commands
- reports the version of the SDP and its components
- reports the status of SDP components
- reports the health state of the system based on the component status


Commands
--------

The commands change the device state as described below, but at present they
have no other effect on SDP.

======= ============= =========== ======
Command Argument type Return type Action
======= ============= =========== ======
On      None          None        Set device state to ON
------- ------------- ----------- ------
Standby None          None        Set device state to STANDBY
------- ------------- ----------- ------
Off     None          None        Set device state to OFF
======= ============= =========== ======


Attributes
----------

Device attributes:

============== ============= ========== ============================== ======================================
Attribute      Type          Read/Write Values                         Description
============== ============= ========== ============================== ======================================
versionId      String        Read       Semantic version               Controller device server version
-------------- ------------- ---------- ------------------------------ --------------------------------------
sdpVersion     String (JSON) Read       :ref:`sdp_version_info`        Versions of the SDP and its components
-------------- ------------- ---------- ------------------------------ --------------------------------------
adminMode      Enum          Read/Write :ref:`controller_admin_mode`   SDP admin mode
-------------- ------------- ---------- ------------------------------ --------------------------------------
healthState    Enum          Read       :ref:`controller_health_state` SDP health state
-------------- ------------- ---------- ------------------------------ --------------------------------------
components     String (JSON) Read       :ref:`controller_components`   Reports information on SDP components
============== ============= ========== ============================== ======================================

Events
^^^^^^

The controller device generates both change events and archive events for the
following attributes:

- State
- versionId
- sdpVersion
- adminMode
- healthState
- components

.. _sdp_version_info:

SDP version information
^^^^^^^^^^^^^^^^^^^^^^^

This attribute contains information about the versions of the SDP and its
components, formatted as a JSON string. The following is an example of the
version information as provided by the SDP Helm chart.

.. code-block:: json

    {
        "version": "0.18.0",
        "components": {
            "etcd": {
                "image": "artefact.skao.int/ska-sdp-etcd"
                "version": "3.5.9"
            },
            "lmc-controller": {
                "devicename": "test-sdp/controller/0",
                "image": "artefact.skao.int/ska-sdp-lmc",
                "version": "0.25.0"
            },
            "lmc-subarray-01": {
                "devicename": "test-sdp/subarray/01",
                "image": "artefact.skao.int/ska-sdp-lmc",
                "version": "0.25.0"
            },
            "lmc-queueconnector-01": {
                "devicename": "test-sdp/queueconnector/01",
                "image": "artefact.skao.int/ska-sdp-lmc-queue-connector",
                "version": "3.0.1"
            },
            "dsconfig": {
                "image": "artefact.skao.int/ska-tango-images-tango-dsconfig",
                "version": "1.5.12"
            },
            "proccontrol": {
                "image": "artefact.skao.int/ska-sdp-proccontrol",
                "version": "0.12.2"
            },
            "helmdeploy": {
                "image": "artefact.skao.int/ska-sdp-helmdeploy",
                "version": "0.14.1"
            },
            "console": {
                "image": "artefact.skao.int/ska-sdp-console",
                "version": "0.5.1"
            }
        },
        "dependencies": {
            "kafka": {
                "repository": "https://charts.bitnami.com/bitnami",
                "version": "23.0.1"
            },
            "ska-sdp-qa": {
                "repository": "https://artefact.skao.int/repository/helm-internal",
                "version": "0.23.2"
            },
            "ska-tango-base": {
                "repository": "https://artefact.skao.int/repository/helm-internal",
                "version": "0.4.7"
            }
        }
    }

.. _controller_admin_mode:

Admin mode values
^^^^^^^^^^^^^^^^^

The admin mode is a writeable attribute which is used to indicate the intended
use of the SDP.

=============== ===========
adminMode       Description
=============== ===========
ONLINE (0)      SDP can be used for normal operations
--------------- -----------
OFFLINE (1)     SDP should not be used
--------------- -----------
ENGINEERING (2) SDP can be used for engineering purposes such as testing, debugging or commissioning
--------------- -----------
NOT_FITTED (3)  Treated as OFFLINE
--------------- -----------
RESERVED (4)    Treated as OFFLINE
=============== ===========

Some of these values are not applicable to a software-only system like the SDP,
but they are implemented for compatibility with the standard interface.

If the admin mode is set to OFFLINE, NOT_FITTED or RESERVED the device state is
set to DISABLE, but there are no other changes to the behaviour of the system.

.. _controller_health_state:

Health state values
^^^^^^^^^^^^^^^^^^^

============ ===========
healthState  Description
============ ===========
OK (0)       SDP is functioning as expected
------------ -----------
DEGRADED (1) SDP can only provide part of its functionality
------------ -----------
FAILED (2)   SDP is unable to function
------------ -----------
UNKNOWN (3)  Controller device is unable to determine the health of the SDP
============ ===========

The health state is based on the status of the SDP components:

- if all components are online: health state is OK
- if the Helm Deployer or the Processing Controller is offline: health state is
  DEGRADED
- if any of the subarrays are offline: health state is DEGRADED
- if all subarrays are offline: health state is FAILED

.. _controller_components:

Components attribute
^^^^^^^^^^^^^^^^^^^^

Reports various information on components in the form of a JSON string.
Currently, it contains status information of the:

- Subarray device(s)
- Processing Controller
- Helm Deployer

When queried, it checks that each component is online i.e. that it is reported
as being "alive" in the configuration database.

Example string:

.. code-block:: json

    {
        "lmc-controller": {"status": "ONLINE"},
        "lmc-subarray-01": {"status": "ONLINE"},
        "lmc-subarray-02": {"status": "OFFLINE",
                            "error": {"SDP Subarray is OFFLINE. It cannot be used for processing."}
                            },
        "proccontrol": {"status": "ONLINE"},
        "helmdeploy": {"status": "OFFLINE",
                       "error": "SDP Helm Deployer is OFFLINE:
                                - Cannot deploy new processing scripts.
                                - Running scripts will not deploy execution engines.
                                - Deployments are not cleaned up."}
                      }
    }
