SDP Local Monitoring and Control
================================

Local Monitoring and Control (LMC) contains the Tango device
servers to control and monitor the Science Data Processor.

.. toctree::
  :maxdepth: 1
  :caption: Releases

  changelog

.. toctree::
  :maxdepth: 1
  :caption: Tango devices

  sdp-controller
  sdp-subarray

.. toctree::
  :maxdepth: 1
  :caption: Developing the LMC

  developing