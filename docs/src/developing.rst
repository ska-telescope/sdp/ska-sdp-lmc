
Developing the LMC
==================

The LMC requires pytango to be installed.

   * On Linux this works as normal.
   * Installation does not work on a Mac at this time.
   * Windows is affected by a bug in the build process and will fail normal
     installation. To circumvent this, install one of the pre-built wheels
     from the pytango site.

In case of problems, docker can be used for linting and testing the code.

Running tests using Docker
--------------------------

Some of the tests require a connection to an etcd database,
which simulates accessing the SDP Configuration Database.
Start the etcd container from the root of ska-sdp-lmc:

.. code-block:: console

    $ ./scripts/docker_run_etcd.sh

While using etcd is optional, since the tests use the memory backend of the
configuration library by default, the container is required to run in order to
start the Python container using the following bash script. Hence, why we need
to start etcd first.

.. code-block:: console

    $ ./scripts/docker_run_python.sh

The above command will also take you inside the container.
Follow the instructions in the message to set up your
environment within the container and start running the tests.
These will also tell you how to run the tests using one
of the `standard make targets <https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile>`_:

.. code-block:: console

    $ make python-test

The tango container is created with a direct connection to
your local code repository. Hence, every change you make using
a text editor or IDE will automatically be accessible from the
docker container.

By default, the tests will use the memory backend of the
configuration library and will use the event loop (which is faster).
To enable the etcd backend and/or the event loop export the following
environment variables within the tango container:

.. code-block:: console

    $ export FEATURE_CONFIG_DB=1
    $ export FEATURE_EVENT_LOOP=1

Linting
-------

The docker container has access to the standard make targets that come
with the repository. A couple of these can be used to lint the code.
You can apply Black and isort to the code with:

.. code-block:: console

    $ make python-format

and you can run the linting checks using:

.. code-block:: console

    $ make python-lint

Cleanup
-------

Once you have finished, you can exit the container,
which will stop automatically.
Don't forget to also stop the etcd container with

.. code-block:: console

    docker stop etcd
