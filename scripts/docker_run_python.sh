#/bin/sh

# Run an Python container for testing code. It connects to the etcd instance
# created by docker_run_etcd.sh.

image=python
version=3.10

echo
echo Starting Python container. The local directory will be mounted in the
echo container.
echo
echo First install the dependencies:
echo  $ pip install poetry
echo  $ poetry config virtualenvs.create false
echo  $ poetry install --no-root
echo
echo Then run the tests:
echo  $ make python-test
echo
echo If you get an error message about the etcd container below, you need to
echo start it first with docker_run_etcd.sh.
echo

docker run -it -v $(pwd):/app -w /app --rm --user root --link etcd \
    --env SDP_CONFIG_HOST=etcd --env SDP_TEST_HOST=etcd --entrypoint bash \
    ${image}:${version}
